<?php

$basePath = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'];

return array(
    'db' => array(
        'driver' => 'Pdo',
        'dsn' => 'mysql:dbname=skeletonapp;host=localhost',
        'user' => 'root',
        'password' => 'w64c74h41',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        ),
    ),
    'view_manager' => array(
        'display_exceptions' => true,
        'base_path' => $basePath,
    ),
    'mail' => array(
        'default' => array(
            'server' => 'mail.ita.locaweb.com.br',
            'port' => 587,
            'username' => 'comunicacao@bridgecoachingprogram.com.br',
            'password' => 'thuch*78',
            'sender' => 'Coaching Program Schneider Electric',
            'replyTo' => 'comunicacao@bridgecoachingprogram.com.br',
        ),
    ),
    'paths' => array(
        'user_photo' => array(
            'base' => "{$basePath}/images/users",
            'path' => 'Z:\WEB\coaching-program\schneider-electric\production\public\images\users',
        ),
        'evaluation-attachment' => array(
            'base' => "{$basePath}/evaluation-attachment",
            'path' => 'Z:\WEB\coaching-program\schneider-electric\production\public\evaluation-attachment',
        ),
        'evaluation-attachment-temp' => array(
            'base' => "{$basePath}/evaluation-attachment/temp",
            'path' => 'Z:\WEB\coaching-program\schneider-electric\production\public\evaluation-attachment\temp',
        ),
    ),
);
