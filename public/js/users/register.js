$(function () {

    $('input[id=fotoFile]').change(function () {
        $('#photoCover').val($(this).val());
    });

    $('#frmUserRegister').bootstrapValidator({
        excluded: ['input:disabled','input:hidden'],
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        }
    });

    $('select[name="id_perfil"]').change(function () {
        if ($(this).val() == 2) {
            $('#panelArea').removeClass('hidden');
            $('#areaagente').attr('disabled', 'disabled');
            $('#areaagente').addClass('hidden');
            $('#areatutor').removeClass('hidden');
        } else if ($(this).val() == 3) {
            $('#panelArea').removeClass('hidden');
            $('#areatutor').attr('disabled', 'disabled');
            $('#areatutor').addClass('hidden');
            $('#areaagente').removeClass('hidden');
        } else {
            $('#panelArea').addClass('hidden');
        }

        $('input[name*="id_area"]').prop('checked', false);
        $('#frmUserRegister').data('bootstrapValidator').validate();
    });

});
