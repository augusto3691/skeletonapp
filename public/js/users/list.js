$(document).ready(function ()
{
    $("#listUser").dataTable({
        "bSort": false,
        language: {
            url: 'https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json'
        }
    }).yadcf([
        {column_number: 0,
            filter_type: 'text',
            style_class: 'form-control',
            filter_default_label: "Id",
        },
        {column_number: 1,
            filter_type: 'text',
            style_class: 'form-control',
            filter_default_label: "Filtrar nome do usuário",
        },
        {column_number: 2,
            style_class: 'form-control',
            filter_default_label: "Filtrar perfil do usuário",
        },
        {column_number: 3,
            filter_type: 'text',
            style_class: 'form-control',
            filter_default_label: "Filtrar e-mail do usuário",
        },
        {column_number: 4,
            filter_type: 'text',
            style_class: 'form-control',
            filter_default_label: "Filtrar data de cadastro",
        },
    ]);

});