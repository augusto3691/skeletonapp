/* 
 * Augusto e Du 12/05/2015
 */

(function ($) {
    $.fn.c3Export = function (options) {

        /********** 
         * Config *
         **********/

        var settings = $.extend({
            filename: "gráfico",
            selector: null
        }, options);

        /*********** 
         * Interno *
         ***********/

        this.on('click', function () {
            createChartImages($(this));
        });
        var styles;
        var createChartImages = function (link) {
            var svg = $(settings.selector).children('svg');
            svg.attr('transform', 'scale(2)');
            $('defs').remove();
            inlineAllStyles();
            var canvas = document.createElement('canvas');
            canvas.width = $(settings.selector).width() * 2;
            canvas.height = $(settings.selector).height() * 2;

            $(settings.selector).append(canvas);

            var canvasContext = canvas.getContext('2d');
            var svg = $.trim(svg[0].outerHTML);
            canvasContext.drawSvg(svg, 0, 0);
            link.attr("href", canvas.toDataURL("png"))
                    .attr("download", function () {
                        return settings.filename + ".png";
                    });
            $(canvas).remove();
        };
        var inlineAllStyles = function () {
            var chartStyle, selector;
            // Get rules from c3.css
            for (var i = 0; i <= document.styleSheets.length - 1; i++) {
                if (document.styleSheets[i].href && document.styleSheets[i].href.indexOf('c3.min.css') !== -1) {
                    if (document.styleSheets[i].rules !== undefined) {
                        chartStyle = document.styleSheets[i].rules;
                    } else {
                        chartStyle = document.styleSheets[i].cssRules;
                    }
                }
            }
            if (chartStyle !== null && chartStyle !== undefined) {
                var changeToDisplay = function () {
                    if ($(this).css('visibility') === 'hidden' || $(this).css('opacity') === '0') {
                        $(this).css('display', 'none');
                    }
                };
                for (i = 0; i < chartStyle.length; i++) {

                    if (chartStyle[i].type === 1) {
                        selector = chartStyle[i].selectorText;
                        styles = makeStyleObject(chartStyle[i]);
                        $('svg *').each(changeToDisplay);
                        $(selector).not($('.c3-chart path')).css(styles);
                    }
                    $('.c3-chart path')
                            .filter(function () {
                                return $(this).css('fill') === 'none';
                            })
                            .attr('fill', 'none');

                    $('.c3-chart path')
                            .filter(function () {
                                return !$(this).css('fill') === 'none';
                            })
                            .attr('fill', function () {
                                return $(this).css('fill');
                            });
                }
            }
        };
        var makeStyleObject = function (rule) {
            var styleDec = rule.style;
            var output = {};
            var s;
            for (s = 0; s < styleDec.length; s++) {
                output[styleDec[s]] = styleDec[styleDec[s]];
            }
            return output;
        };
        return this;
    };
}(jQuery));

