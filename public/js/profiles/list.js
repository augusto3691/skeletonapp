$(document).ready(function ()
{
    $("#listProfile").dataTable({
        "bSort": false,
        language: {
            url: 'https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json'
        }
    }).yadcf([
        {column_number: 0,
            filter_type: 'text',
            style_class: 'form-control',
            filter_default_label: "Id",
        },
        {column_number: 1,
            filter_type: 'text',
            style_class: 'form-control',
            filter_default_label: "Filtrar nome do perfil",
        },
        {column_number: 2,
            filter_type: 'text',
            style_class: 'form-control',
            filter_default_label: "Filtrar descrição do perfil",
        },
        {column_number: 3,
            filter_type: 'text',
            style_class: 'form-control',
            filter_default_label: "Filtrar data de cadastro",
        },
    ]);

});