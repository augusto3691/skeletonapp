$(function () {

    $('#treePermissions').treegrid();
    $('#treePermissions').treegrid('collapseAll');

    var grantParents = function (node) {
        var parentNode = node.treegrid('getParentNode');

        if (parentNode) {
            parentNode.find('input:radio').val(['1']);

            var buttons = parentNode.find('div.btn-group');
            buttons.find('label:first').addClass('active');
            buttons.find('label:last').removeClass('active');

            grantParents(parentNode);
        }
    };

    var denyChildrens = function (node) {
        var childrenNodes = node.treegrid('getChildNodes');
        childrenNodes.each(function (index, childNode) {
            childNode = $(childNode);
            childNode.find('input:radio').val(['0']);

            var buttons = childNode.find('div.btn-group');
            buttons.find('label:first').removeClass('active');
            buttons.find('label:last').addClass('active');

            if (!childNode.treegrid('isLeaf')) {
                denyChildrens(childNode);
            }
        });
    };

    $(".btn").click(function () {
        var node = $(this).parents('tr');

        if ($(this).hasClass('allow')) {
            grantParents(node);
        } else {
            denyChildrens(node);
        }

    });
});