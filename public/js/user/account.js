$(function() {

    $('input[id=fotoFile]').change(function() {
        $('#photoCover').val($(this).val());
    });

    $('#frmUserData').bootstrapValidator({
        excluded: [':disabled'],
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        }
    });

});