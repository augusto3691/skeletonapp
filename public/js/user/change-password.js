$(function() {

    $('#formChangePassword').bootstrapValidator({
        excluded: [':disabled'],
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            newCredential: {
                validators: {
                    stringLength: {
                        min: 6,
                        max: 12
                    },
                    identical: {
                        field: 'newCredentialConfirm'
                    }
                }
            },
            newCredentialConfirm: {
                validators: {
                    stringLength: {
                        min: 6,
                        max: 12
                    },
                    identical: {
                        field: 'newCredential'
                    }
                }
            }
        }
    });

});