$(function() {

    $('#formCreatePassword').bootstrapValidator({
        excluded: [':disabled'],
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            credential: {
                validators: {
                    stringLength: {
                        min: 6,
                        max: 12
                    },
                    identical: {
                        field: 'credentialConfirm'
                    }
                }
            },
            credentialConfirm: {
                validators: {
                    stringLength: {
                        min: 6,
                        max: 12
                    },
                    identical: {
                        field: 'credential'
                    }
                }
            }
        }
    });

});