-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 24-Jan-2018 às 14:53
-- Versão do servidor: 10.1.29-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sim`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_log`
--

CREATE TABLE `tb_log` (
  `id_LogOperacoes` int(11) NOT NULL,
  `dt_Data` datetime(6) NOT NULL,
  `id_Participante` int(11) DEFAULT NULL,
  `id_LogOperacoesNivel` int(11) NOT NULL,
  `ch_Tipo` varchar(100) NOT NULL,
  `ch_Evento` varchar(45) NOT NULL,
  `ch_DadosAdicionais` longtext,
  `ch_IP` varchar(20) NOT NULL DEFAULT '-'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_log`
--

INSERT INTO `tb_log` (`id_LogOperacoes`, `dt_Data`, `id_Participante`, `id_LogOperacoesNivel`, `ch_Tipo`, `ch_Evento`, `ch_DadosAdicionais`, `ch_IP`) VALUES
(0, '2017-02-03 14:32:03.000000', 88, 0, 'EMERG', '505', '{\"error\":\"Exception:n1: Statement could not be executed (42S02 - 1146 - Table \\u0027skeletonapp.list\\u0027 doesn\\u0027t exist)\\n2: SQLSTATE[42S02]: Base table or view not found: 1146 Table \\u0027skeletonapp.list\\u0027 doesn\\u0027t exist\\nTrace:\\n#0 C:\\\\xampp\\\\htdocs\\\\zf2\\\\vendor\\\\zendframework\\\\zendframework\\\\library\\\\Zend\\\\Db\\\\TableGateway\\\\AbstractTableGateway.php(233): Zend\\\\Db\\\\Adapter\\\\Driver\\\\Pdo\\\\Statement-\\u003Eexecute()\\n#1 C:\\\\xampp\\\\htdocs\\\\zf2\\\\vendor\\\\zendframework\\\\zendframework\\\\library\\\\Zend\\\\Db\\\\TableGateway\\\\AbstractTableGateway.php(208): Zend\\\\Db\\\\TableGateway\\\\AbstractTableGateway-\\u003EexecuteSelect(Object(Zend\\\\Db\\\\Sql\\\\Select))\\n#2 C:\\\\xampp\\\\htdocs\\\\zf2\\\\vendor\\\\zendframework\\\\zendframework\\\\library\\\\Zend\\\\Db\\\\TableGateway\\\\AbstractTableGateway.php(195): Zend\\\\Db\\\\TableGateway\\\\AbstractTableGateway-\\u003EselectWith(Object(Zend\\\\Db\\\\Sql\\\\Select))\\n#3 C:\\\\xampp\\\\htdocs\\\\zf2\\\\module\\\\Application\\\\src\\\\Controller\\\\Index.php(22): Zend\\\\Db\\\\TableGateway\\\\AbstractTableGateway-\\u003Eselect()\\n#4 C:\\\\xampp\\\\htdocs\\\\zf2\\\\vendor\\\\zendframework\\\\zendframework\\\\library\\\\Zend\\\\Mvc\\\\Controller\\\\AbstractActionController.php(83): Application\\\\Controller\\\\Index-\\u003EhomeAction()\\n#5 [internal function]: Zend\\\\Mvc\\\\Controller\\\\AbstractActionController-\\u003EonDispatch(Object(Zend\\\\Mvc\\\\MvcEvent))\\n#6 C:\\\\xampp\\\\htdocs\\\\zf2\\\\vendor\\\\zendframework\\\\zendframework\\\\library\\\\Zend\\\\EventManager\\\\EventManager.php(468): call_user_func(Array, Object(Zend\\\\Mvc\\\\MvcEvent))\\n#7 C:\\\\xampp\\\\htdocs\\\\zf2\\\\vendor\\\\zendframework\\\\zendframework\\\\library\\\\Zend\\\\EventManager\\\\EventManager.php(207): Zend\\\\EventManager\\\\EventManager-\\u003EtriggerListeners(\\u0027dispatch\\u0027, Object(Zend\\\\Mvc\\\\MvcEvent), Object(Closure))\\n#8 C:\\\\xampp\\\\htdocs\\\\zf2\\\\vendor\\\\zendframework\\\\zendframework\\\\library\\\\Zend\\\\Mvc\\\\Controller\\\\AbstractController.php(116): Zend\\\\EventManager\\\\EventManager-\\u003Etrigger(\\u0027dispatch\\u0027, Object(Zend\\\\Mvc\\\\MvcEvent), Object(Closure))\\n#9 C:\\\\xampp\\\\htdocs\\\\zf2\\\\vendor\\\\zendframework\\\\zendframework\\\\library\\\\Zend\\\\Mvc\\\\DispatchListener.php(113): Zend\\\\Mvc\\\\Controller\\\\AbstractController-\\u003Edispatch(Object(Zend\\\\Http\\\\PhpEnvironment\\\\Request), Object(Zend\\\\Http\\\\PhpEnvironment\\\\Response))\\n#10 [internal function]: Zend\\\\Mvc\\\\DispatchListener-\\u003EonDispatch(Object(Zend\\\\Mvc\\\\MvcEvent))\\n#11 C:\\\\xampp\\\\htdocs\\\\zf2\\\\vendor\\\\zendframework\\\\zendframework\\\\library\\\\Zend\\\\EventManager\\\\EventManager.php(468): call_user_func(Array, Object(Zend\\\\Mvc\\\\MvcEvent))\\n#12 C:\\\\xampp\\\\htdocs\\\\zf2\\\\vendor\\\\zendframework\\\\zendframework\\\\library\\\\Zend\\\\EventManager\\\\EventManager.php(207): Zend\\\\EventManager\\\\EventManager-\\u003EtriggerListeners(\\u0027dispatch\\u0027, Object(Zend\\\\Mvc\\\\MvcEvent), Object(Closure))\\n#13 C:\\\\xampp\\\\htdocs\\\\zf2\\\\vendor\\\\zendframework\\\\zendframework\\\\library\\\\Zend\\\\Mvc\\\\Application.php(313): Zend\\\\EventManager\\\\EventManager-\\u003Etrigger(\\u0027dispatch\\u0027, Object(Zend\\\\Mvc\\\\MvcEvent), Object(Closure))\\n#14 C:\\\\xampp\\\\htdocs\\\\zf2\\\\public\\\\index.php(20): Zend\\\\Mvc\\\\Application-\\u003Erun()\\n#15 {main}\"}', '127.0.0.1'),
(0, '2018-01-18 14:09:56.000000', 88, 6, 'INFO', 'LOGIN', NULL, '127.0.0.1'),
(0, '2018-01-22 16:48:36.000000', 88, 6, 'INFO', 'LOGIN', NULL, '127.0.0.1'),
(0, '2018-01-23 13:07:01.000000', 88, 6, 'INFO', 'PROFILE_REGISTER', '{\"profile\":{\"id\":3,\"dataCadastro\":{\"date\":\"2018-01-23 13:07:01.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"\",\"descricao\":\"                                \",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 13:07:01.000000', 88, 6, 'INFO', 'PROFILE_REGISTER', '{\"profile\":{\"id\":4,\"dataCadastro\":{\"date\":\"2018-01-23 13:07:01.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"\",\"descricao\":\"                                \",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 13:07:01.000000', 88, 6, 'INFO', 'PROFILE_REGISTER', '{\"profile\":{\"id\":5,\"dataCadastro\":{\"date\":\"2018-01-23 13:07:01.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"\",\"descricao\":\"                                \",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 13:07:02.000000', 88, 6, 'INFO', 'PROFILE_REGISTER', '{\"profile\":{\"id\":6,\"dataCadastro\":{\"date\":\"2018-01-23 13:07:02.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"\",\"descricao\":\"                                \",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 13:18:49.000000', 88, 6, 'INFO', 'PROFILE_REGISTER', '{\"profile\":{\"id\":7,\"dataCadastro\":{\"date\":\"2018-01-23 13:18:49.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"\",\"descricao\":\"                                \",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 13:18:58.000000', 88, 6, 'INFO', 'PROFILE_REGISTER', '{\"profile\":{\"id\":8,\"dataCadastro\":{\"date\":\"2018-01-23 13:18:58.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"\",\"descricao\":\"                                \",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 14:59:05.000000', 88, 6, 'INFO', 'PROFILE_REGISTER', '{\"profile\":{\"id\":9,\"dataCadastro\":{\"date\":\"2018-01-23 14:59:05.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"\",\"descricao\":\"                                \",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 14:59:20.000000', 88, 6, 'INFO', 'PROFILE_REGISTER', '{\"profile\":{\"id\":10,\"dataCadastro\":{\"date\":\"2018-01-23 14:59:20.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"\",\"descricao\":\"                                \",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 15:03:59.000000', 88, 6, 'INFO', 'PROFILE_REGISTER', '{\"profile\":{\"id\":11,\"dataCadastro\":{\"date\":\"2018-01-23 15:03:59.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"AA\",\"descricao\":\"       AA                         \",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 16:19:19.000000', 88, 6, 'INFO', 'PROFILE_REGISTER', '{\"profile\":{\"id\":12,\"dataCadastro\":{\"date\":\"2018-01-23 16:19:19.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"AA\",\"descricao\":\"           a                     \",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 16:20:58.000000', 88, 6, 'INFO', 'PROFILE_REGISTER', '{\"profile\":{\"id\":13,\"dataCadastro\":{\"date\":\"2018-01-23 16:20:58.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"ANd\",\"descricao\":\"                     asdasd           \",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 16:22:36.000000', 88, 6, 'INFO', 'PROFILE_REGISTER', '{\"profile\":{\"id\":14,\"dataCadastro\":{\"date\":\"2018-01-23 16:22:36.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"hfdhdf\",\"descricao\":\"                hdfghdfg                \",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 16:25:32.000000', 88, 6, 'INFO', 'PROFILE_REGISTER', '{\"profile\":{\"id\":15,\"dataCadastro\":{\"date\":\"2018-01-23 16:25:32.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"jfgj\",\"descricao\":\"             fgjhfgj                   \",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 16:26:33.000000', 88, 6, 'INFO', 'PROFILE_REGISTER', '{\"profile\":{\"id\":16,\"dataCadastro\":{\"date\":\"2018-01-23 16:26:33.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"sdfgsdf\",\"descricao\":\"                   gsdfgsdfg             \",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 16:27:16.000000', 88, 6, 'INFO', 'PROFILE_REGISTER', '{\"profile\":{\"id\":17,\"dataCadastro\":{\"date\":\"2018-01-23 16:27:16.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"asdfasdf\",\"descricao\":\"         asdfasf                       \",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 16:27:57.000000', 88, 6, 'INFO', 'PROFILE_REGISTER', '{\"profile\":{\"id\":18,\"dataCadastro\":{\"date\":\"2018-01-23 16:27:57.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"asdfasdfasdf\",\"descricao\":\"asdfasdfasdf                                \",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 16:29:52.000000', 88, 6, 'INFO', 'PROFILE_REGISTER', '{\"profile\":{\"id\":19,\"dataCadastro\":{\"date\":\"2018-01-23 16:29:52.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"1111111111111\",\"descricao\":\"123321321          \",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 16:30:59.000000', 88, 6, 'INFO', 'PROFILE_REGISTER', '{\"profile\":{\"id\":20,\"dataCadastro\":{\"date\":\"2018-01-23 16:30:59.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"fdas\",\"descricao\":\"               fasdfasdf                 \",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 16:34:23.000000', 88, 6, 'INFO', 'PROFILE_REGISTER', '{\"profile\":{\"id\":21,\"dataCadastro\":{\"date\":\"2018-01-23 16:34:23.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"dasdasd\",\"descricao\":\"                 asdas               \",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 16:34:29.000000', 88, 6, 'INFO', 'PROFILE_REGISTER', '{\"profile\":{\"id\":22,\"dataCadastro\":{\"date\":\"2018-01-23 16:34:29.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"asdasd\",\"descricao\":\"     sadasd                           \",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 16:35:17.000000', 88, 6, 'INFO', 'PROFILE_REGISTER', '{\"profile\":{\"id\":23,\"dataCadastro\":{\"date\":\"2018-01-23 16:35:17.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"asdfasdf\",\"descricao\":\"     asdfasdf                           \",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 16:38:27.000000', 88, 6, 'INFO', 'PROFILE_REGISTER', '{\"profile\":{\"id\":24,\"dataCadastro\":{\"date\":\"2018-01-23 16:38:27.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"teste\",\"descricao\":\"teste         \",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 16:39:02.000000', 88, 6, 'INFO', 'PROFILE_REGISTER', '{\"profile\":{\"id\":25,\"dataCadastro\":{\"date\":\"2018-01-23 16:39:02.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"teste\",\"descricao\":\"                teste                \",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 16:39:08.000000', 88, 6, 'INFO', 'PROFILE_REGISTER', '{\"profile\":{\"id\":26,\"dataCadastro\":{\"date\":\"2018-01-23 16:39:08.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"fasfsa\",\"descricao\":\"         fasdfasd                       \",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 16:42:10.000000', 88, 0, 'EMERG', '505', '{\"error\":\"Exception:n1: Statement could not be executed (42S02 - 1146 - Table \\u0027sim.list\\u0027 doesn\\u0027t exist)\\n2: SQLSTATE[42S02]: Base table or view not found: 1146 Table \\u0027sim.list\\u0027 doesn\\u0027t exist\\nTrace:\\n#0 C:\\\\xampp\\\\htdocs\\\\sim\\\\vendor\\\\zendframework\\\\zendframework\\\\library\\\\Zend\\\\Db\\\\TableGateway\\\\AbstractTableGateway.php(233): Zend\\\\Db\\\\Adapter\\\\Driver\\\\Pdo\\\\Statement-\\u003Eexecute()\\n#1 C:\\\\xampp\\\\htdocs\\\\sim\\\\vendor\\\\zendframework\\\\zendframework\\\\library\\\\Zend\\\\Db\\\\TableGateway\\\\AbstractTableGateway.php(208): Zend\\\\Db\\\\TableGateway\\\\AbstractTableGateway-\\u003EexecuteSelect(Object(Zend\\\\Db\\\\Sql\\\\Select))\\n#2 C:\\\\xampp\\\\htdocs\\\\sim\\\\vendor\\\\zendframework\\\\zendframework\\\\library\\\\Zend\\\\Db\\\\TableGateway\\\\AbstractTableGateway.php(195): Zend\\\\Db\\\\TableGateway\\\\AbstractTableGateway-\\u003EselectWith(Object(Zend\\\\Db\\\\Sql\\\\Select))\\n#3 C:\\\\xampp\\\\htdocs\\\\sim\\\\module\\\\Application\\\\src\\\\Controller\\\\Index.php(18): Zend\\\\Db\\\\TableGateway\\\\AbstractTableGateway-\\u003Eselect()\\n#4 C:\\\\xampp\\\\htdocs\\\\sim\\\\vendor\\\\zendframework\\\\zendframework\\\\library\\\\Zend\\\\Mvc\\\\Controller\\\\AbstractActionController.php(83): Application\\\\Controller\\\\Index-\\u003EhomeAction()\\n#5 [internal function]: Zend\\\\Mvc\\\\Controller\\\\AbstractActionController-\\u003EonDispatch(Object(Zend\\\\Mvc\\\\MvcEvent))\\n#6 C:\\\\xampp\\\\htdocs\\\\sim\\\\vendor\\\\zendframework\\\\zendframework\\\\library\\\\Zend\\\\EventManager\\\\EventManager.php(468): call_user_func(Array, Object(Zend\\\\Mvc\\\\MvcEvent))\\n#7 C:\\\\xampp\\\\htdocs\\\\sim\\\\vendor\\\\zendframework\\\\zendframework\\\\library\\\\Zend\\\\EventManager\\\\EventManager.php(207): Zend\\\\EventManager\\\\EventManager-\\u003EtriggerListeners(\\u0027dispatch\\u0027, Object(Zend\\\\Mvc\\\\MvcEvent), Object(Closure))\\n#8 C:\\\\xampp\\\\htdocs\\\\sim\\\\vendor\\\\zendframework\\\\zendframework\\\\library\\\\Zend\\\\Mvc\\\\Controller\\\\AbstractController.php(116): Zend\\\\EventManager\\\\EventManager-\\u003Etrigger(\\u0027dispatch\\u0027, Object(Zend\\\\Mvc\\\\MvcEvent), Object(Closure))\\n#9 C:\\\\xampp\\\\htdocs\\\\sim\\\\vendor\\\\zendframework\\\\zendframework\\\\library\\\\Zend\\\\Mvc\\\\DispatchListener.php(113): Zend\\\\Mvc\\\\Controller\\\\AbstractController-\\u003Edispatch(Object(Zend\\\\Http\\\\PhpEnvironment\\\\Request), Object(Zend\\\\Http\\\\PhpEnvironment\\\\Response))\\n#10 [internal function]: Zend\\\\Mvc\\\\DispatchListener-\\u003EonDispatch(Object(Zend\\\\Mvc\\\\MvcEvent))\\n#11 C:\\\\xampp\\\\htdocs\\\\sim\\\\vendor\\\\zendframework\\\\zendframework\\\\library\\\\Zend\\\\EventManager\\\\EventManager.php(468): call_user_func(Array, Object(Zend\\\\Mvc\\\\MvcEvent))\\n#12 C:\\\\xampp\\\\htdocs\\\\sim\\\\vendor\\\\zendframework\\\\zendframework\\\\library\\\\Zend\\\\EventManager\\\\EventManager.php(207): Zend\\\\EventManager\\\\EventManager-\\u003EtriggerListeners(\\u0027dispatch\\u0027, Object(Zend\\\\Mvc\\\\MvcEvent), Object(Closure))\\n#13 C:\\\\xampp\\\\htdocs\\\\sim\\\\vendor\\\\zendframework\\\\zendframework\\\\library\\\\Zend\\\\Mvc\\\\Application.php(313): Zend\\\\EventManager\\\\EventManager-\\u003Etrigger(\\u0027dispatch\\u0027, Object(Zend\\\\Mvc\\\\MvcEvent), Object(Closure))\\n#14 C:\\\\xampp\\\\htdocs\\\\sim\\\\public\\\\index.php(20): Zend\\\\Mvc\\\\Application-\\u003Erun()\\n#15 {main}\"}', '127.0.0.1'),
(0, '2018-01-23 16:44:42.000000', 88, 6, 'INFO', 'PROFILE_UPDATE', '{\"profile\":{\"id\":2,\"dataCadastro\":{\"date\":\"2016-09-08 15:58:57.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Guest\",\"descricao\":\"Guest Role for test\",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 16:46:39.000000', 88, 6, 'INFO', 'PROFILE_UPDATE', '{\"profile\":{\"id\":2,\"dataCadastro\":{\"date\":\"2016-09-08 15:58:57.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Guest\",\"descricao\":\"Guest Role\",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 16:51:04.000000', 88, 6, 'INFO', 'LOGIN', NULL, '127.0.0.1'),
(0, '2018-01-23 19:53:47.000000', 88, 6, 'INFO', 'USER_UPDATE', '{\"user\":{\"id\":88,\"idPerfil\":1,\"labelPerfil\":null,\"dataCadastro\":{\"date\":\"2014-11-24 17:48:19.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"sobrenome\":\"Admin\",\"apelido\":\"Admin\",\"email\":\"admin@yopmail.com\",\"credencial\":\"\\/YERBkFOeuyLAtmj3pbxTg==\",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 19:53:55.000000', 88, 6, 'INFO', 'USER_UPDATE', '{\"user\":{\"id\":88,\"idPerfil\":1,\"labelPerfil\":null,\"dataCadastro\":{\"date\":\"2014-11-24 17:48:19.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"sobrenome\":\"Admin\",\"apelido\":\"Admin\",\"email\":\"admin@yopmail.com\",\"credencial\":\"\\/YERBkFOeuyLAtmj3pbxTg==\",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 19:54:10.000000', 88, 6, 'INFO', 'USER_UPDATE', '{\"user\":{\"id\":88,\"idPerfil\":1,\"labelPerfil\":null,\"dataCadastro\":{\"date\":\"2014-11-24 17:48:19.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"sobrenome\":\"Admin\",\"apelido\":\"Admin\",\"email\":\"admin@yopmail.com\",\"credencial\":\"\\/YERBkFOeuyLAtmj3pbxTg==\",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 19:54:14.000000', 88, 6, 'INFO', 'USER_UPDATE', '{\"user\":{\"id\":89,\"idPerfil\":1,\"labelPerfil\":null,\"dataCadastro\":{\"date\":\"2016-09-08 12:22:13.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Guest\",\"sobrenome\":\"Guest\",\"apelido\":\"Guest\",\"email\":\"guest@yopmail.com\",\"credencial\":\"\\/YERBkFOeuyLAtmj3pbxTg==\",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 20:44:29.000000', 88, 6, 'INFO', 'PROFILE_UPDATE', '{\"profile\":{\"id\":1,\"dataCadastro\":{\"date\":\"2016-09-08 15:58:57.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"descricao\":\"Admin Role\",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 20:44:41.000000', 88, 6, 'INFO', 'PROFILE_UPDATE', '{\"profile\":{\"id\":1,\"dataCadastro\":{\"date\":\"2016-09-08 15:58:57.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"descricao\":\"Admin Role\",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 20:44:51.000000', 88, 6, 'INFO', 'USER_UPDATE', '{\"user\":{\"id\":88,\"idPerfil\":1,\"labelPerfil\":null,\"dataCadastro\":{\"date\":\"2014-11-24 17:48:19.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"sobrenome\":\"Admin\",\"apelido\":\"Admin\",\"email\":\"admin@yopmail.com\",\"credencial\":\"\\/YERBkFOeuyLAtmj3pbxTg==\",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 20:45:03.000000', 88, 6, 'INFO', 'PROFILE_UPDATE', '{\"profile\":{\"id\":1,\"dataCadastro\":{\"date\":\"2016-09-08 15:58:57.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"descricao\":\"Admin Role\",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 20:45:47.000000', 88, 6, 'INFO', 'PROFILE_UPDATE', '{\"profile\":{\"id\":1,\"dataCadastro\":{\"date\":\"2016-09-08 15:58:57.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"descricao\":\"Admin\",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-23 20:45:54.000000', 88, 6, 'INFO', 'PROFILE_UPDATE', '{\"profile\":{\"id\":1,\"dataCadastro\":{\"date\":\"2016-09-08 15:58:57.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"descricao\":\"Admin Role\",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-24 12:13:46.000000', 88, 6, 'INFO', 'PROFILE_PERMISSION', '{\"profile\":{\"id\":2,\"dataCadastro\":{\"date\":\"2016-09-08 15:58:57.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Guest\",\"descricao\":\"Guest Role\",\"ativo\":true},\"aclList\":[{\"id\":694,\"idPerfil\":2,\"action\":\"allow\",\"resource\":\"app\\/manage\"},{\"id\":695,\"idPerfil\":2,\"action\":\"allow\",\"resource\":\"app\\/manage\\/clean-cache\"},{\"id\":696,\"idPerfil\":2,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\"},{\"id\":697,\"idPerfil\":2,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/register\"},{\"id\":698,\"idPerfil\":2,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/edit\"},{\"id\":699,\"idPerfil\":2,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/permissions\"},{\"id\":700,\"idPerfil\":2,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\"},{\"id\":701,\"idPerfil\":2,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\\/register\"},{\"id\":702,\"idPerfil\":2,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\\/edit\"},{\"id\":703,\"idPerfil\":2,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\\/remove\"}]}', '127.0.0.1'),
(0, '2018-01-24 12:13:54.000000', 88, 6, 'INFO', 'PROFILE_PERMISSION', '{\"profile\":{\"id\":1,\"dataCadastro\":{\"date\":\"2016-09-08 15:58:57.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"descricao\":\"Admin Role\",\"ativo\":true},\"aclList\":[{\"id\":704,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\"},{\"id\":705,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/clean-cache\"},{\"id\":706,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\"},{\"id\":707,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/register\"},{\"id\":708,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/edit\"},{\"id\":709,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/permissions\"},{\"id\":710,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\"},{\"id\":711,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\\/register\"},{\"id\":712,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\\/edit\"},{\"id\":713,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\\/remove\"}]}', '127.0.0.1'),
(0, '2018-01-24 12:14:03.000000', 88, 6, 'INFO', 'PROFILE_UPDATE', '{\"profile\":{\"id\":1,\"dataCadastro\":{\"date\":\"2016-09-08 15:58:57.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"descricao\":\"Admin Role\",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-24 12:14:58.000000', 88, 6, 'INFO', 'PROFILE_PERMISSION', '{\"profile\":{\"id\":1,\"dataCadastro\":{\"date\":\"2016-09-08 15:58:57.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"descricao\":\"Admin Role\",\"ativo\":true},\"aclList\":[{\"id\":714,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\"},{\"id\":715,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/clean-cache\"},{\"id\":716,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\"},{\"id\":717,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/register\"},{\"id\":718,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/edit\"},{\"id\":719,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/permissions\"},{\"id\":720,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\"},{\"id\":721,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\\/register\"},{\"id\":722,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\\/edit\"},{\"id\":723,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\\/remove\"}]}', '127.0.0.1'),
(0, '2018-01-24 12:15:53.000000', 88, 6, 'INFO', 'PROFILE_PERMISSION', '{\"profile\":{\"id\":1,\"dataCadastro\":{\"date\":\"2016-09-08 15:58:57.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"descricao\":\"Admin Role\",\"ativo\":true},\"aclList\":[{\"id\":724,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\"},{\"id\":725,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/clean-cache\"},{\"id\":726,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\"},{\"id\":727,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/register\"},{\"id\":728,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/edit\"},{\"id\":729,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/permissions\"}]}', '127.0.0.1'),
(0, '2018-01-24 12:16:37.000000', 88, 6, 'INFO', 'PROFILE_PERMISSION', '{\"profile\":{\"id\":1,\"dataCadastro\":{\"date\":\"2016-09-08 15:58:57.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"descricao\":\"Admin Role\",\"ativo\":true},\"aclList\":[{\"id\":730,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\"},{\"id\":731,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/clean-cache\"},{\"id\":732,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\"},{\"id\":733,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/register\"},{\"id\":734,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/edit\"},{\"id\":735,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/permissions\"},{\"id\":736,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\"}]}', '127.0.0.1'),
(0, '2018-01-24 12:17:06.000000', 88, 6, 'INFO', 'PROFILE_PERMISSION', '{\"profile\":{\"id\":1,\"dataCadastro\":{\"date\":\"2016-09-08 15:58:57.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"descricao\":\"Admin Role\",\"ativo\":true},\"aclList\":[{\"id\":737,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\"},{\"id\":738,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/clean-cache\"},{\"id\":739,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\"},{\"id\":740,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/register\"},{\"id\":741,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/edit\"},{\"id\":742,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/permissions\"}]}', '127.0.0.1'),
(0, '2018-01-24 12:31:36.000000', 88, 6, 'INFO', 'PROFILE_PERMISSION', '{\"profile\":{\"id\":1,\"dataCadastro\":{\"date\":\"2016-09-08 15:58:57.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"descricao\":\"Admin Role\",\"ativo\":true},\"aclList\":[{\"id\":743,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\"},{\"id\":744,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/clean-cache\"},{\"id\":745,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\"},{\"id\":746,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/register\"},{\"id\":747,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/edit\"},{\"id\":748,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/permissions\"},{\"id\":749,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\"}]}', '127.0.0.1'),
(0, '2018-01-24 12:32:01.000000', 88, 6, 'INFO', 'PROFILE_PERMISSION', '{\"profile\":{\"id\":1,\"dataCadastro\":{\"date\":\"2016-09-08 15:58:57.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"descricao\":\"Admin Role\",\"ativo\":true},\"aclList\":[{\"id\":750,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\"},{\"id\":751,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/clean-cache\"},{\"id\":752,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\"},{\"id\":753,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/register\"},{\"id\":754,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/edit\"},{\"id\":755,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/permissions\"}]}', '127.0.0.1'),
(0, '2018-01-24 12:32:15.000000', 88, 6, 'INFO', 'PROFILE_PERMISSION', '{\"profile\":{\"id\":1,\"dataCadastro\":{\"date\":\"2016-09-08 15:58:57.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"descricao\":\"Admin Role\",\"ativo\":true},\"aclList\":[{\"id\":756,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\"},{\"id\":757,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/clean-cache\"},{\"id\":758,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\"},{\"id\":759,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/register\"},{\"id\":760,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/edit\"},{\"id\":761,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/permissions\"},{\"id\":762,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\"},{\"id\":763,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\\/register\"},{\"id\":764,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\\/edit\"},{\"id\":765,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\\/remove\"}]}', '127.0.0.1'),
(0, '2018-01-24 12:32:24.000000', 88, 6, 'INFO', 'PROFILE_PERMISSION', '{\"profile\":{\"id\":1,\"dataCadastro\":{\"date\":\"2016-09-08 15:58:57.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"descricao\":\"Admin Role\",\"ativo\":true},\"aclList\":[{\"id\":766,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\"},{\"id\":767,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/clean-cache\"},{\"id\":768,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\"},{\"id\":769,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/register\"},{\"id\":770,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/edit\"},{\"id\":771,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/permissions\"}]}', '127.0.0.1'),
(0, '2018-01-24 12:32:59.000000', 88, 6, 'INFO', 'PROFILE_PERMISSION', '{\"profile\":{\"id\":1,\"dataCadastro\":{\"date\":\"2016-09-08 15:58:57.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"descricao\":\"Admin Role\",\"ativo\":true},\"aclList\":[{\"id\":772,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\"},{\"id\":773,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/clean-cache\"},{\"id\":774,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\"},{\"id\":775,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/register\"},{\"id\":776,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/edit\"},{\"id\":777,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/permissions\"},{\"id\":778,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\"},{\"id\":779,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\\/register\"},{\"id\":780,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\\/edit\"},{\"id\":781,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\\/remove\"}]}', '127.0.0.1'),
(0, '2018-01-24 12:33:12.000000', 88, 6, 'INFO', 'PROFILE_PERMISSION', '{\"profile\":{\"id\":1,\"dataCadastro\":{\"date\":\"2016-09-08 15:58:57.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"descricao\":\"Admin Role\",\"ativo\":true},\"aclList\":[{\"id\":782,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\"},{\"id\":783,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/clean-cache\"},{\"id\":784,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\"},{\"id\":785,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/register\"},{\"id\":786,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/edit\"},{\"id\":787,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/permissions\"},{\"id\":788,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\"}]}', '127.0.0.1'),
(0, '2018-01-24 12:33:28.000000', 88, 6, 'INFO', 'PROFILE_PERMISSION', '{\"profile\":{\"id\":1,\"dataCadastro\":{\"date\":\"2016-09-08 15:58:57.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"descricao\":\"Admin Role\",\"ativo\":true},\"aclList\":[{\"id\":789,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\"},{\"id\":790,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/clean-cache\"},{\"id\":791,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\"},{\"id\":792,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/register\"},{\"id\":793,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/edit\"},{\"id\":794,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/profiles\\/permissions\"},{\"id\":795,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\"},{\"id\":796,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\\/register\"},{\"id\":797,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\\/edit\"},{\"id\":798,\"idPerfil\":1,\"action\":\"allow\",\"resource\":\"app\\/manage\\/users\\/remove\"}]}', '127.0.0.1'),
(0, '2018-01-24 14:15:28.000000', 88, 6, 'INFO', 'USER_UPDATE', '{\"user\":{\"id\":88,\"idPerfil\":1,\"labelPerfil\":\"Admin\",\"dataCadastro\":{\"date\":\"2014-11-24 17:48:19.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"sobrenome\":\"Admin\",\"apelido\":\"Admin\",\"email\":\"admin@yopmail.com\",\"credencial\":\"\\/YERBkFOeuyLAtmj3pbxTg==\",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-24 14:16:36.000000', 88, 6, 'INFO', 'USER_UPDATE', '{\"user\":{\"id\":88,\"idPerfil\":1,\"labelPerfil\":\"Admin\",\"dataCadastro\":{\"date\":\"2014-11-24 17:48:19.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"sobrenome\":\"Admin\",\"apelido\":\"Admin\",\"email\":\"admin@yopmail.com\",\"credencial\":\"\\/YERBkFOeuyLAtmj3pbxTg==\",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-24 14:16:44.000000', 88, 6, 'INFO', 'USER_UPDATE', '{\"user\":{\"id\":88,\"idPerfil\":1,\"labelPerfil\":\"Admin\",\"dataCadastro\":{\"date\":\"2014-11-24 17:48:19.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"sobrenome\":\"Admin\",\"apelido\":\"Admin\",\"email\":\"admin@yopmail.com\",\"credencial\":\"\\/YERBkFOeuyLAtmj3pbxTg==\",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-24 14:23:54.000000', 88, 6, 'INFO', 'USER_UPDATE', '{\"user\":{\"id\":88,\"idPerfil\":1,\"labelPerfil\":\"Admin\",\"dataCadastro\":{\"date\":\"2014-11-24 17:48:19.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"sobrenome\":\"Admin\",\"apelido\":\"Admin\",\"email\":\"admin@yopmail.com\",\"credencial\":\"\\/YERBkFOeuyLAtmj3pbxTg==\",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-24 14:23:54.000000', 88, 6, 'INFO', 'USER_UPDATE', '{\"user\":{\"id\":88,\"idPerfil\":1,\"labelPerfil\":\"Admin\",\"dataCadastro\":{\"date\":\"2014-11-24 17:48:19.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"sobrenome\":\"Admin\",\"apelido\":\"Admin\",\"email\":\"admin@yopmail.com\",\"credencial\":\"\\/YERBkFOeuyLAtmj3pbxTg==\",\"ativo\":true}}', '127.0.0.1'),
(0, '2018-01-24 14:23:54.000000', 88, 6, 'INFO', 'USER_UPDATE', '{\"user\":{\"id\":88,\"idPerfil\":1,\"labelPerfil\":\"Admin\",\"dataCadastro\":{\"date\":\"2014-11-24 17:48:19.000000\",\"timezone_type\":3,\"timezone\":\"Europe\\/Berlin\"},\"nome\":\"Admin\",\"sobrenome\":\"Admin\",\"apelido\":\"Admin\",\"email\":\"admin@yopmail.com\",\"credencial\":\"\\/YERBkFOeuyLAtmj3pbxTg==\",\"ativo\":true}}', '127.0.0.1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_log_operations`
--

CREATE TABLE `tb_log_operations` (
  `id_LogOperacoesNivel` int(11) NOT NULL,
  `ch_Tipo` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_log_operations`
--

INSERT INTO `tb_log_operations` (`id_LogOperacoesNivel`, `ch_Tipo`) VALUES
(0, 'Emergency: system is unusable'),
(1, 'Alert: action must be taken immediately'),
(2, 'Critical: critical conditions'),
(3, 'Error: error conditions'),
(4, 'Warning: warning conditions'),
(5, 'Notice: normal but significant condition'),
(6, 'Informational: informational messages'),
(7, 'Debug: debug-level messages');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_role`
--

CREATE TABLE `tb_role` (
  `id_Perfil` int(11) NOT NULL,
  `ch_Perfil` varchar(40) NOT NULL,
  `ch_Descricao` longtext,
  `flg_Inativo` tinyint(1) NOT NULL,
  `dt_Inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_Atualizacao` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_role`
--

INSERT INTO `tb_role` (`id_Perfil`, `ch_Perfil`, `ch_Descricao`, `flg_Inativo`, `dt_Inclusao`, `dt_Atualizacao`) VALUES
(1, 'Admin', 'Admin Role', 0, '2016-09-08 18:58:57', NULL),
(2, 'Guest', 'Guest Role', 0, '2016-09-08 18:58:57', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_role_acl`
--

CREATE TABLE `tb_role_acl` (
  `id_Perfil_Acl` int(11) NOT NULL,
  `id_Perfil` int(11) NOT NULL,
  `ch_Acao` varchar(20) NOT NULL,
  `ch_Recurso` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_role_acl`
--

INSERT INTO `tb_role_acl` (`id_Perfil_Acl`, `id_Perfil`, `ch_Acao`, `ch_Recurso`) VALUES
(694, 2, 'allow', 'app/manage'),
(695, 2, 'allow', 'app/manage/clean-cache'),
(696, 2, 'allow', 'app/manage/profiles'),
(697, 2, 'allow', 'app/manage/profiles/register'),
(698, 2, 'allow', 'app/manage/profiles/edit'),
(699, 2, 'allow', 'app/manage/profiles/permissions'),
(700, 2, 'allow', 'app/manage/users'),
(701, 2, 'allow', 'app/manage/users/register'),
(702, 2, 'allow', 'app/manage/users/edit'),
(703, 2, 'allow', 'app/manage/users/remove'),
(789, 1, 'allow', 'app/manage'),
(790, 1, 'allow', 'app/manage/clean-cache'),
(791, 1, 'allow', 'app/manage/profiles'),
(792, 1, 'allow', 'app/manage/profiles/register'),
(793, 1, 'allow', 'app/manage/profiles/edit'),
(794, 1, 'allow', 'app/manage/profiles/permissions'),
(795, 1, 'allow', 'app/manage/users'),
(796, 1, 'allow', 'app/manage/users/register'),
(797, 1, 'allow', 'app/manage/users/edit'),
(798, 1, 'allow', 'app/manage/users/remove');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_token`
--

CREATE TABLE `tb_token` (
  `ch_Servico` varchar(20) NOT NULL,
  `ch_Chave` varchar(160) NOT NULL,
  `ch_Token` char(32) NOT NULL,
  `dt_Expiracao` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_token`
--

INSERT INTO `tb_token` (`ch_Servico`, `ch_Chave`, `ch_Token`, `dt_Expiracao`) VALUES
('FPASSWD', 'admin@yopmail.com', '4148049634c2dcdea5802a0ffcdbfc10', '2017-02-08 13:57:57.000000'),
('REGISTERPASSWD', 'guest@guest.com.br', '728fe6628dce9ba7dfb0527c7911af6e', '2016-12-07 12:22:13.000000');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_users`
--

CREATE TABLE `tb_users` (
  `id_Participante` int(11) NOT NULL,
  `id_Perfil` int(11) DEFAULT NULL,
  `ch_Nome` varchar(200) NOT NULL,
  `ch_SobreNome` varchar(200) DEFAULT NULL,
  `ch_Apelido` varchar(100) DEFAULT NULL,
  `ch_Email` varchar(100) NOT NULL,
  `ch_Senha` varchar(50) DEFAULT NULL,
  `num_CPF` bigint(20) DEFAULT NULL,
  `dt_Nascimento` datetime(6) DEFAULT NULL,
  `flg_Inativo` tinyint(1) NOT NULL,
  `flg_CadastroCompleto` tinyint(1) NOT NULL DEFAULT '0',
  `flg_AcessoSite` tinyint(1) NOT NULL DEFAULT '0',
  `dt_Inclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dt_Atualizacao` datetime(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_users`
--

INSERT INTO `tb_users` (`id_Participante`, `id_Perfil`, `ch_Nome`, `ch_SobreNome`, `ch_Apelido`, `ch_Email`, `ch_Senha`, `num_CPF`, `dt_Nascimento`, `flg_Inativo`, `flg_CadastroCompleto`, `flg_AcessoSite`, `dt_Inclusao`, `dt_Atualizacao`) VALUES
(88, 1, 'Admin', 'Admin', 'Admin', 'admin@yopmail.com', '/YERBkFOeuyLAtmj3pbxTg==', NULL, NULL, 0, 1, 0, '2014-11-24 19:48:19', NULL),
(89, 1, 'Guest', 'Guest', 'Guest', 'guest@yopmail.com', '/YERBkFOeuyLAtmj3pbxTg==', NULL, NULL, 0, 1, 0, '2016-09-08 15:22:13', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_log_operations`
--
ALTER TABLE `tb_log_operations`
  ADD PRIMARY KEY (`id_LogOperacoesNivel`);

--
-- Indexes for table `tb_role`
--
ALTER TABLE `tb_role`
  ADD PRIMARY KEY (`id_Perfil`);

--
-- Indexes for table `tb_role_acl`
--
ALTER TABLE `tb_role_acl`
  ADD PRIMARY KEY (`id_Perfil_Acl`),
  ADD KEY `FK__tb_Perfil__id_Pe__78F3E6EC` (`id_Perfil`);

--
-- Indexes for table `tb_token`
--
ALTER TABLE `tb_token`
  ADD PRIMARY KEY (`ch_Servico`,`ch_Chave`,`ch_Token`);

--
-- Indexes for table `tb_users`
--
ALTER TABLE `tb_users`
  ADD PRIMARY KEY (`id_Participante`),
  ADD UNIQUE KEY `Unq_tb_Participantes_ch_Email` (`ch_Email`),
  ADD KEY `FK_tb_Participantes_Perfil` (`id_Perfil`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_role`
--
ALTER TABLE `tb_role`
  MODIFY `id_Perfil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_role_acl`
--
ALTER TABLE `tb_role_acl`
  MODIFY `id_Perfil_Acl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=799;

--
-- AUTO_INCREMENT for table `tb_users`
--
ALTER TABLE `tb_users`
  MODIFY `id_Participante` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
