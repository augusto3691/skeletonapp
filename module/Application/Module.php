<?php

namespace Application;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface
{

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/',
                ),
            ),
        );
    }

    public function getConfig()
    {
        return array_merge_recursive(
            include __DIR__ . '/config/module.config.php', 
            include __DIR__ . '/config/module.routes.php', 
            include __DIR__ . '/config/module.logger.php', 
            include __DIR__ . '/config/module.navigation.php'
        );
    }

    public function onBootstrap(MvcEvent $event)
    {
        $eventManager = $event->getApplication()->getEventManager();

        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        //Locale (intl)
        $eventManager->attach(MvcEvent::EVENT_ROUTE, array(new Event\Locale, 'route'));

        //Autenticação
        $authEvent = new Event\Authentication;
        $authEvent->setUnauthorizedRoute('app/user/authenticate');
        $authEvent->setAuthenticationParameter('authentication');
        $authEvent->setRedirectParameter('redirect');
        $eventManager->attach(MvcEvent::EVENT_ROUTE, array($authEvent, 'route'));

        //Permissão
        $permissionEvent = new Event\Permission;
        $permissionEvent->setDeniedRoute('app/user/access-denied');
        $eventManager->attach(MvcEvent::EVENT_ROUTE, array($permissionEvent, 'route'));

        //Logger
        $loggerEvent = new Event\Logger;
        $loggerEvent->setServiceLocator($event->getApplication()->getServiceManager());
        $eventManager->attachAggregate($loggerEvent);

    }

}
