<?php

return array(
    'doctrine' => array(
        'driver' => array(
            // defines an annotation driver with two paths, and names it `my_annotation_driver`
            'my_annotation_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . "/src/Application/Entity"
                ),
            ),
            // default metadata driver, aggregates all other drivers into a single one.
            // Override `orm_default` only if you know what you're doing
            'orm_default' => array(
                'drivers' => array(
                    // register `my_annotation_driver` for any entity under namespace `My\Namespace`
                    'Application\Entity' => 'my_annotation_driver'
                )
            )
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'app' => 'Application\Controller\Index',
            'app/user' => 'Application\Controller\User',
            'app/profiles' => 'Application\Controller\Profiles',
            'app/users' => 'Application\Controller\Users',
        ),
    ),
    'controller_plugins' => array(
        'invokables' => array(
            'Messages' => 'Application\Controller\Plugin\Messages',
            'Translate' => 'Application\Controller\Plugin\Translate',
        ),
    ),
    'service_manager' => array(
        'aliases' => array(
            'dbAdapter' => 'Zend\Db\Adapter\Adapter',
            'translator' => 'mvctranslator',
            'AuthService' => 'Zend\Authentication\AuthenticationService',
            'AclService' => 'Zend\Permissions\Acl\Acl',
        ),
        'invokables' => array(
            'LogService' => 'Application\Service\Log',
            'MailService' => 'Application\Service\Mail',
            'TokenService' => 'Application\Service\Token',
            'Application\Service\User' => 'Application\Service\User',
            'Application\Service\Profile' => 'Application\Service\Profile',
        ),
        'factories' => array(
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
            'CacheStorage' => 'Application\Service\CacheStorageFactory',
            'Zend\Authentication\AuthenticationService' => 'Application\Service\AuthenticationFactory',
            'Zend\Permissions\Acl\Acl' => 'Application\Service\AclFactory',
            'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory',
            'OrderItemMapper' => function($sm) {
                $orderTable = new \Application\Entity\OrderTable($sm->get('dbAdapter'));
                $itemTable = new \Application\Entity\OrderItemTable($sm->get('dbAdapter'));
                $mapper = new Application\Entity\OrderItemMapper($orderTable, $itemTable);
                return $mapper;
            },
        ),
    ),
    'view_manager' => array(
        'doctype' => 'HTML5',
        'exception_template' => 'error/500',
        'display_not_found_reason' => true,
        'not_found_template' => 'error/404',
        'template_map' => array(
            'layout/layout' => __DIR__ . '/../view/layout/default.phtml',
            'layout/login' => __DIR__ . '/../view/layout/login.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/500' => __DIR__ . '/../view/error/500.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    'view_helpers' => array(
        'invokables' => array(
            'getConfig' => 'Application\View\Helper\GetConfig',
            'UserHeader' => 'Application\View\Helper\UserHeader',
            'isAllowed' => 'Application\View\Helper\IsAllowed',
            'currencyFormat' => 'Application\View\Helper\CurrencyFormat',
            'InputFilterToBootstrapValidator' => 'Application\View\Helper\InputFilterToBootstrapValidator',
            'AlertClass' => 'Application\View\Helper\AlertClass',
        )
    ),
    'translator' => array(
        'locale' => 'pt-BR',
        'translation_file_patterns' => array(
            array(
                'type' => 'phpArray',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.php',
            ),
        ),
    ),
);
