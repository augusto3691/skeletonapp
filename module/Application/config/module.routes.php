<?php

return array(
    'router' => array(
        'routes' => array(
            'app' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/:locale',
                    'constraints' => array(
                        'locale' => '[a-zA-Z-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'app',
                        'action' => 'home',
                        'authentication' => 'AuthService',
                        'permission' => false,
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'keep-alive' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/keep-alive',
                            'defaults' => array(
                                'controller' => 'app',
                                'action' => 'keep-alive',
                                'authentication' => false,
                                'permission' => false,
                            ),
                        ),
                    ),
                    'user' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/user',
                            'defaults' => array(
                                'controller' => 'app/user',
                                'action' => 'account',
                                'authentication' => 'AuthService',
                                'permission' => false,
                            ),
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(
                            'authenticate' => array(
                                'type' => 'Literal',
                                'options' => array(
                                    'route' => '/authenticate',
                                    'defaults' => array(
                                        'controller' => 'app/user',
                                        'action' => 'authenticate',
                                        'authentication' => false,
                                        'permission' => false,
                                    ),
                                ),
                            ),
                            'logout' => array(
                                'type' => 'Literal',
                                'options' => array(
                                    'route' => '/logout',
                                    'defaults' => array(
                                        'controller' => 'app/user',
                                        'action' => 'logout',
                                        'authentication' => false,
                                        'permission' => false,
                                    ),
                                ),
                            ),
                            'create-password' => array(
                                'type' => 'Segment',
                                'options' => array(
                                    'route' => '/create-password/:token',
                                    'constrainsts' => array(
                                        'token' => '[A-Za-z0-9]{32}',
                                    ),
                                    'defaults' => array(
                                        'controller' => 'app/user',
                                        'action' => 'create-password',
                                        'authentication' => false,
                                        'permission' => false,
                                    ),
                                ),
                            ),
                            'forgot-password' => array(
                                'type' => 'Literal',
                                'options' => array(
                                    'route' => '/forgot-password',
                                    'defaults' => array(
                                        'controller' => 'app/user',
                                        'action' => 'forgot-password',
                                        'authentication' => false,
                                        'permission' => false,
                                    ),
                                ),
                            ),
                            'reset-password' => array(
                                'type' => 'Segment',
                                'options' => array(
                                    'route' => '/reset-password/:token',
                                    'constrainsts' => array(
                                        'token' => '[A-Za-z0-9]{32}',
                                    ),
                                    'defaults' => array(
                                        'controller' => 'app/user',
                                        'action' => 'reset-password',
                                        'authentication' => false,
                                        'permission' => false,
                                    ),
                                ),
                            ),
                            'change-password' => array(
                                'type' => 'Literal',
                                'options' => array(
                                    'route' => '/change-password',
                                    'defaults' => array(
                                        'controller' => 'app/user',
                                        'action' => 'change-password',
                                        'authentication' => 'AuthService',
                                        'permission' => false,
                                    ),
                                ),
                            ),
                            'access-denied' => array(
                                'type' => 'Literal',
                                'options' => array(
                                    'route' => '/access-denied',
                                    'defaults' => array(
                                        'controller' => 'app/user',
                                        'action' => 'access-denied',
                                        'authentication' => 'AuthService',
                                        'permission' => false,
                                    ),
                                ),
                            ),
                        ),
                    ),
                    'manage' => array(
                        'type' => 'Literal',
                        'options' => array(
                            'route' => '/manage',
                            'defaults' => array(
                                'controller' => 'app',
                                'action' => 'navigation',
                                'authentication' => 'AuthService',
                                'permission' => true,
                            ),
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(
                            'clean-cache' => array(
                                'type' => 'Literal',
                                'options' => array(
                                    'route' => '/clean-cache',
                                    'defaults' => array(
                                        'controller' => 'app',
                                        'action' => 'clean-cache',
                                        'authentication' => 'AuthService',
                                        'permission' => true,
                                    ),
                                ),
                            ),
                            'profiles' => array(
                                'type' => 'Literal',
                                'options' => array(
                                    'route' => '/profiles',
                                    'defaults' => array(
                                        'controller' => 'app/profiles',
                                        'action' => 'list',
                                        'authentication' => 'AuthService',
                                        'permission' => true,
                                    ),
                                ),
                                'may_terminate' => true,
                                'child_routes' => array(
                                    'register' => array(
                                        'type' => 'Literal',
                                        'options' => array(
                                            'route' => '/register',
                                            'defaults' => array(
                                                'controller' => 'app/profiles',
                                                'action' => 'register',
                                                'authentication' => 'AuthService',
                                                'permission' => true,
                                            ),
                                        ),
                                    ),
                                    'edit' => array(
                                        'type' => 'Segment',
                                        'options' => array(
                                            'route' => '/edit/:idPerfil',
                                            'constraints' => array(
                                                'idPerfil' => '[0-9]+',
                                            ),
                                            'defaults' => array(
                                                'controller' => 'app/profiles',
                                                'action' => 'update',
                                                'authentication' => 'AuthService',
                                                'permission' => true,
                                            ),
                                        ),
                                    ),
                                    'permissions' => array(
                                        'type' => 'Segment',
                                        'options' => array(
                                            'route' => '/permissions/:idPerfil',
                                            'constraints' => array(
                                                'idPerfil' => '[0-9]+',
                                            ),
                                            'defaults' => array(
                                                'controller' => 'app/profiles',
                                                'action' => 'permission',
                                                'authentication' => 'AuthService',
                                                'permission' => true,
                                            ),
                                        ),
                                    ),
                                ),
                            ),
                            'users' => array(
                                'type' => 'Literal',
                                'options' => array(
                                    'route' => '/users',
                                    'defaults' => array(
                                        'controller' => 'app/users',
                                        'action' => 'list',
                                        'authentication' => 'AuthService',
                                        'permission' => true,
                                    ),
                                ),
                                'may_terminate' => true,
                                'child_routes' => array(
                                    'register' => array(
                                        'type' => 'Literal',
                                        'options' => array(
                                            'route' => '/register',
                                            'defaults' => array(
                                                'controller' => 'app/users',
                                                'action' => 'register',
                                                'authentication' => 'AuthService',
                                                'permission' => true,
                                            ),
                                        ),
                                    ),
                                    'edit' => array(
                                        'type' => 'Segment',
                                        'options' => array(
                                            'route' => '/edit/:idUsuario',
                                            'constraints' => array(
                                                'idUsuario' => '[0-9]+',
                                            ),
                                            'defaults' => array(
                                                'controller' => 'app/users',
                                                'action' => 'update',
                                                'authentication' => 'AuthService',
                                                'permission' => true,
                                            ),
                                        ),
                                    ),
                                    'remove' => array(
                                        'type' => 'Segment',
                                        'options' => array(
                                            'route' => '/remover/:idUsuario',
                                            'constraints' => array(
                                                'idUsuario' => '[0-9]+',
                                            ),
                                            'defaults' => array(
                                                'controller' => 'app/users',
                                                'action' => 'remove',
                                                'authentication' => 'AuthService',
                                                'permission' => true,
                                            ),
                                        ),
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
);
