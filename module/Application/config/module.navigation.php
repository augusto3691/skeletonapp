<?php

return array(
    'navigation' => array(
        'default' => array(
            array(
                'label' => 'Cadastro',
                'route' => 'app/manage',
                'resource' => 'app/manage',
                'icon' => 'fa fa-plus-circle',
                'pages' => array(
                    array(
                        'label' => 'Fornecedores',
                        'route' => 'app/manage/clean-cache',
                        'resource' => 'app/manage/clean-cache',
                    ),
                    array(
                        'label' => 'Filiais',
                        'route' => 'app/manage/profiles',
                        'resource' => 'app/manage/profiles',
                    ),
                    array(
                        'label' => 'Usuários',
                        'route' => 'app/manage/users',
                        'resource' => 'app/manage/users',
                    ),
                ),
            ),
            array(
                'label' => 'Marketing',
                'route' => 'app/manage',
                'resource' => 'app/manage',
                'icon' => 'fa fa-bullhorn',
                'pages' => array(
                    array(
                        'label' => 'Verbas',
                        'route' => 'app/manage/clean-cache',
                        'resource' => 'app/manage/clean-cache',
                    ),
                    array(
                        'label' => 'Fluxo',
                        'route' => 'app/manage/profiles',
                        'resource' => 'app/manage/profiles',
                    ),
                    array(
                        'label' => 'Solicitar Ação',
                        'route' => 'app/manage/users',
                        'resource' => 'app/manage/users',
                    ),
                ),
            ),
            array(
                'label' => 'Gerenciar',
                'route' => 'app/manage',
                'resource' => 'app/manage',
                'icon' => 'fa fa-cog',
                'pages' => array(
                    array(
                        'label' => 'Limpar cache',
                        'route' => 'app/manage/clean-cache',
                        'resource' => 'app/manage/clean-cache',
                    ),
                    array(
                        'label' => 'Perfis',
                        'route' => 'app/manage/profiles',
                        'resource' => 'app/manage/profiles',
                    ),
                    array(
                        'label' => 'Usuários',
                        'route' => 'app/manage/users',
                        'resource' => 'app/manage/users',
                    ),
                ),
            ),
            array(
                'label' => 'Relatórios',
                'route' => 'app/manage',
                'resource' => 'app/manage',
                'icon' => 'fa fa-area-chart',
                'pages' => array(
                    array(
                        'label' => 'Relatorio 1',
                        'route' => 'app/manage/clean-cache',
                        'resource' => 'app/manage/clean-cache',
                    ),
                ),
            ),
        ),
    ),
);
