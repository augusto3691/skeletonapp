<?php

use Application\Service\Profile;
use Application\Service\User;

return array(
    'logger' => array(
        'Application\Service\Profile' => array(
            Profile::EVENT_REGISTER_POS => 'PROFILE_REGISTER',
            Profile::EVENT_UPDATE_POS => 'PROFILE_UPDATE',
            Profile::EVENT_SET_PERMISSIONS_POS => 'PROFILE_PERMISSION',
        ),
        'Application\Service\User' => array(
            User::EVENT_REGISTER_POS => 'USER_REGISTER',
            User::EVENT_UPDATE_POS => 'USER_UPDATE',
        ),
    ),
);
