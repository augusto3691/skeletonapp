<?php

return array(
    'app/manage' => 'Gerenciar',
    'app/manage/profiles' => 'Perfis',
    'app/manage/profiles/register' => 'Cadastrar',
    'app/manage/profiles/edit' => 'Editar',
    'app/manage/profiles/permissions' => 'Permissões',
    'app/manage/users' => 'Usuários',
    'app/manage/users/register' => 'Cadastrar',
    'app/manage/users/edit' => 'Editar',
    'app/manage/users/remove' => 'Inativar',
    'app/manage/clean-cache' => 'Limpar Cache',
);
