<?php

namespace Application\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

/**
 * @ControllerPlugin
 * 
 * @author Augusto Coelho
 */
class Translate extends AbstractPlugin
{

    public function __invoke($message, $textDomain = 'default', $locale = null)
    {
        $translator = $this->getController()->getServiceLocator()->get('translator');
        return $translator->translate($message, $textDomain, $locale);
    }

}
