<?php

namespace Application\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use IteratorAggregate;
use Countable;
use ArrayIterator;
use Zend\Session\Container as SessionContainer;

/**
 * @ControllerPlugin
 *
 * @author Augusto Coelho
 */
class Messages extends AbstractPlugin implements IteratorAggregate, Countable
{

    const TYPE_SUCCESS = 'alert-success';
    const TYPE_INFO = 'alert-info';
    const TYPE_WARNING = 'alert-warning';
    const TYPE_DANGER = 'alert-danger';

    /**
     * @var \Zend\Session\Container
     */
    protected $session = null;

    /**
     * @return \Zend\Session\Container
     */
    public function getSession()
    {
        if (null == $this->session) {
            $this->session = new SessionContainer('Messages');
        }
        return $this->session;
    }

    /**
     * @param string $message
     * @param string $type <success, info, warning, danger>
     * @return \Application\Controller\Plugin\Messages
     */
    public function add($message, $type = self::INFO)
    {
        $messages = $this->getSession()->offsetGet('messages');
        $messages[$type][] = (string)$message;
        $this->getSession()->offsetSet('messages', $messages);

        return $this;
    }

    /**
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        $messages = $this->getSession()->offsetGet('messages');
        $this->getSession()->offsetUnset('messages');

        return new ArrayIterator($messages ?: array());
    }

    /**
     * @return integer
     */
    public function count()
    {
        return $this->getSession()->getIterator()->count();
    }

}
