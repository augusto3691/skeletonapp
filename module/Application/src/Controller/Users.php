<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Controller\Plugin\Messages;
use Zend\Stdlib\Hydrator\ClassMethods as HydratorClassMethods;

/**
 * @Controller
 *
 * @author Augusto Coelho
 */
class Users extends AbstractActionController
{

    public function listAction()
    {
        $viewModel = new ViewModel;

        $dbAdapter = $this->getServiceLocator()->get('dbAdapter');
        $cacheStorage = $this->getServiceLocator()->get('CacheStorage');

        $perfilTable = new \Application\Model\PerfilTable($dbAdapter);
        $perfilTable->setCacheStorage($cacheStorage);
        $viewModel->setVariable('perfilList', $perfilTable->fetchAll()->toArray());

        $usuarioTable = new \Application\Model\UsuarioTable($dbAdapter);
        $usuarioTable->setCacheStorage($cacheStorage);
        $viewModel->setVariable('usuarioList', $usuarioTable->fetchAll()->toArray());

        return $viewModel;
    }

    public function registerAction()
    {
        $viewModel = $this->registerViewForm();
        $usuarioEntity = new \Application\Model\Usuario;
        $dbAdapter = $this->getServiceLocator()->get('dbAdapter');

        if ($this->getRequest()->isPost()) {
            $hydrator = new HydratorClassMethods;
            $usuarioEntity = $hydrator->hydrate($this->params()->fromPost(), $usuarioEntity);
            try {
                $userService = $this->getServiceLocator()->get('Application\Service\User');
                $userService->register($usuarioEntity);
                $userService->receiveUploadPhoto($usuarioEntity, 'foto');

                $message = $this->translate('Usuario <strong>%s</strong> cadastrado com sucesso');
                $this->Messages()->add(sprintf($message, $usuarioEntity->getNome()), Messages::TYPE_SUCCESS);

                return $this->redirect()->toRoute('app/manage/users');
            } catch (\Application\Service\Exception\UploadException $e) {
                $this->Messages()->add($e->getMessage(), Messages::TYPE_DANGER);
            } catch (\Exception $e) {
                $this->Messages()->add($this->translate('Não foi possível cadastrar o usuário'), Messages::TYPE_DANGER);
            }
        }

        $viewModel->setVariable('usuario', $usuarioEntity);
        return $viewModel;
    }

    public function updateAction()
    {
        $viewModel = $this->registerViewForm();
        $idUsuario = $this->params('idUsuario');
        $config = $this->getServiceLocator()->get('Application')->getConfig();

        $dbAdapter = $this->getServiceLocator()->get('dbAdapter');
        $cacheStorage = $this->getServiceLocator()->get('CacheStorage');
        $userTable = new \Application\Model\UsuarioTable($dbAdapter);
        $userTable->setCacheStorage($cacheStorage);

        $userEntity = $userTable->findById($idUsuario);

        if (file_exists("{$config['paths']['user_photo']['path']}/{$userEntity->getId()}.jpg")) {
            $photoPath = "{$config['paths']['user_photo']['base']}/{$userEntity->getId()}.jpg";
        } else {
            $photoPath = "{$config['paths']['user_photo']['base']}/no-user.jpg";
        }

        if ($this->getRequest()->isPost()) {
            $hydrator = new HydratorClassMethods;
            $userEntity = $hydrator->hydrate($this->params()->fromPost(), $userEntity);

            try {
                $userService = $this->getServiceLocator()->get('Application\Service\User');
                $userService->update($userEntity);
                $userService->receiveUploadPhoto($userEntity, 'foto');

                $message = $this->translate('Usuario <strong>%s</strong> alterado com sucesso');
                $this->Messages()->add(sprintf($message, $userEntity->getNome()), Messages::TYPE_SUCCESS);

                return $this->redirect()->toRoute('app/manage/users');
            } catch (\Application\Service\Exception\UploadException $e) {
                $this->Messages()->add($e->getMessage(), Messages::TYPE_DANGER);
            } catch (\Exception $e) {
                $this->Messages()->add($this->translate('Não foi possível alterar o usuário'), Messages::TYPE_DANGER);
            }
        }

        $viewModel->setVariable('usuario', $userEntity);
        $viewModel->setVariable('userPhoto', $photoPath);
        return $viewModel;
    }

    public function removeAction()
    {
        $viewModel = $this->registerViewForm();
        $idUsuario = $this->params('idUsuario');

        $dbAdapter = $this->getServiceLocator()->get('dbAdapter');
        $userTable = new \Application\Model\UsuarioTable($dbAdapter);
        $userEntity = $userTable->findById($idUsuario);

        $connection = $dbAdapter->getDriver()->getConnection();
        $ativo = false;

        try {
            $connection->beginTransaction();
            $userEntity->setAtivo($ativo);
            $userTable->updateEntity($userEntity);
            $connection->commit();
            $message = $this->translate('Usuario <strong>%s</strong> alterado com sucesso.');
            $this->Messages()->add(sprintf($message, $userEntity->getNome()), Messages::TYPE_SUCCESS);
            return $this->redirect()->toRoute('app/manage/users');
        } catch (\Exception $e) {
            $connection->rollback();
            $this->Messages()->add($this->translate('Não foi possível alterar o usuário.'), Messages::TYPE_DANGER);
        }
    }

    protected function registerViewForm()
    {
        $viewModel = new ViewModel;
        $viewModel->setTemplate('application/users/register');

        $dbAdapter = $this->getServiceLocator()->get('dbAdapter');
        $cacheStorage = $this->getServiceLocator()->get('CacheStorage');

        $perfilTable = new \Application\Model\PerfilTable($dbAdapter);
        $perfilTable->setCacheStorage($cacheStorage);
        $viewModel->setVariable('perfilList', $perfilTable->findByAtivos());

        return $viewModel;
    }

}
