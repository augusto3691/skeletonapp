<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Controller\Plugin\Messages;

/**
 * @Controller
 *
 * @author Augusto Coelho
 */
class Profiles extends AbstractActionController
{

    public function listAction()
    {
        $viewModel = new ViewModel;

        $dbAdapter = $this->getServiceLocator()->get('dbAdapter');
        $cacheStorage = $this->getServiceLocator()->get('CacheStorage');

        $perfilTable = new \Application\Model\PerfilTable($dbAdapter);
        $perfilTable->setCacheStorage($cacheStorage);
        $viewModel->setVariable('perfilList', $perfilTable->fetchAll()->toArray());

        return $viewModel;
    }

    public function registerAction()
    {
        $viewModel = new ViewModel;
        $perfilEntity = new \Application\Model\Perfil;

        if ($this->getRequest()->isPost()) {
            $hydrator = new \Zend\Stdlib\Hydrator\ClassMethods;
            $perfilEntity = $hydrator->hydrate($this->params()->fromPost(), $perfilEntity);
            $perfilEntity->setDataCadastro(new \DateTime);

            $profileService = $this->getServiceLocator()->get('Application\Service\Profile');
            if ($profileService->register($perfilEntity)) {
                $message = $this->translate('Perfil <strong>%s</strong> cadastrado com sucesso');
                $this->Messages()->add(sprintf($message, $perfilEntity->getNome()), Messages::TYPE_SUCCESS);

                return $this->redirect()->toRoute('app/manage/profiles');
            } else {
                $this->Messages()->add($this->translate('Não foi possível cadastrar o Perfil'), Messages::TYPE_WARNING);
            }
        }

        $viewModel->setVariable('perfil', $perfilEntity);
        return $viewModel;
    }

    public function updateAction()
    {
        $viewModel = new ViewModel;
        $viewModel->setTemplate('application/profiles/register');

        $idPerfil = $this->params('idPerfil');
        $dbAdapter = $this->getServiceLocator()->get('dbAdapter');
        $cacheStorage = $this->getServiceLocator()->get('CacheStorage');

        $perfilTable = new \Application\Model\PerfilTable($dbAdapter);
        $perfilTable->setCacheStorage($cacheStorage);
        $perfilEntity = $perfilTable->findById($idPerfil);

        if ($this->getRequest()->isPost()) {
            $hydrator = new \Zend\Stdlib\Hydrator\ClassMethods;
            $perfilEntity = $hydrator->hydrate($this->params()->fromPost(), $perfilEntity);

            $profileService = $this->getServiceLocator()->get('Application\Service\Profile');
            if ($profileService->update($perfilEntity)) {
                $message = $this->translate('Perfil <strong>%s</strong> alterado com sucesso');
                $this->Messages()->add(sprintf($message, $perfilEntity->getNome()), Messages::TYPE_SUCCESS);

                return $this->redirect()->toRoute('app/manage/profiles');
            } else {
                $this->Messages()->add($this->translate('Não foi possível alterar o Perfil'), Messages::TYPE_WARNING);
            }
        }

        $viewModel->setVariable('perfil', $perfilEntity);
        return $viewModel;
    }

    public function permissionAction()
    {
        $viewModel = new ViewModel;

        $dbAdapter = $this->getServiceLocator()->get('dbAdapter');
        $cacheStorage = $this->getServiceLocator()->get('CacheStorage');

        $perfilTable = new \Application\Model\PerfilTable($dbAdapter);
        $perfilTable->setCacheStorage($cacheStorage);
        $perfilEntity = $perfilTable->findById($this->params('idPerfil'));

        if (!$perfilEntity) {
            return $this->redirect()->toRoute('profile');
        }

        $aclService = $this->getServiceLocator()->get('AclService');
        $role = "profile-{$perfilEntity->getId()}";

        if ($this->getRequest()->isPost()) {
            $permissionList = array();
            foreach ($this->params()->fromPost('resource') as $resource => $permission) {
                if ((bool)$permission == true) {
                    $aclEntity = new \Application\Model\Perfil\Acl;
                    $aclEntity->setAction(\Application\Model\Perfil\Acl::ACTION_ALLOW)
                        ->setResource($resource);
                    $permissionList[] = $aclEntity;
                }
            }

            $profileService = $this->getServiceLocator()->get('Application\Service\Profile');
            if ($profileService->setPermissions($perfilEntity, $permissionList)) {
                $message = $this->translate('Permissões do Perfil <strong>%s</strong> salvas com sucesso');
                $this->Messages()->add(sprintf($message, $perfilEntity->getNome()), Messages::TYPE_SUCCESS);

                return $this->redirect()->toRoute('app/manage/profiles');
            } else {
                $this->Messages()->add($this->translate('Não foi possível alterar as permissões do Perfil'), Messages::TYPE_DANGER);
            }
        }

        $resourceList = array();
        $nodeIndexList = array('');
        $routerLabels = include realpath(__DIR__ . '/../../config/router-labels.php');
        foreach ($aclService->getResources() as $resource) {
            $nodeIdName = str_replace('/', '_', str_replace('app/', '', $resource));
            $nodeParentName = substr($nodeIdName, 0, strrpos($nodeIdName, '_'));

            $nodeId = array_search($nodeIdName, $nodeIndexList);
            if ($nodeId === false) {
                $nodeId = array_push($nodeIndexList, $nodeIdName) - 1;
            }

            if ($nodeParentName) {
                $nodeParentId = array_search($nodeParentName, $nodeIndexList);
                if ($nodeParentId === false) {
                    $nodeParentId = array_push($nodeIndexList, $nodeParentName) - 1;
                }
            } else {
                $nodeParentId = null;
            }

            $resourceList[] = array(
                'nodeId' => "treegrid-{$nodeId}",
                'nodeParent' => $nodeParentId ? "treegrid-parent-{$nodeParentId}" : null,
                'resource' => $resource,
                'resourceLabel' => isset($routerLabels[$resource]) ? $this->translate($routerLabels[$resource]) : $resource,
                'permission' => $aclService->isAllowed($role, $resource),
            );
        }

        $viewModel->setVariable('perfil', $perfilEntity);
        $viewModel->setVariable('resourceList', $resourceList);
        $viewModel->setVariable('idPerfil', $this->params('idPerfil'));

        return $viewModel;
    }

}
