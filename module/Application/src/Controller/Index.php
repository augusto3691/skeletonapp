<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Controller\Plugin\Messages;

class Index extends AbstractActionController
{

    public function homeAction()
    {
        $viewModel = new ViewModel();

        $adapter = $this->getServiceLocator()->get('dbAdapter');

        return $viewModel;
    }

    public function navigationAction()
    {
        $viewModel = new ViewModel;

        $route = $this->getEvent()->getRouteMatch()->getMatchedRouteName();
        $viewModel->setVariable('route', $route);

        return $viewModel;
    }

    public function keepAliveAction()
    {
        $response = new \Zend\Http\Response;

        $response->getHeaders()->addHeaderLine('Content-type', 'text/plain');
        $response->setContent('survive');

        return $response;
    }

    public function cleanCacheAction()
    {
        $cacheStorage = $this->getServiceLocator()->get('CacheStorage');

        try {
            $cacheStorage->clearExpired();
            $cacheStorage->clearByNamespace($cacheStorage->getOptions()->getNamespace());

            $this->Messages()->add($this->translate('Cache limpo com sucesso'), Messages::TYPE_SUCCESS);
        } catch (\Exception $e) {
            $this->Messages()->add($this->translate('Não foi possível limpar o cache'), Messages::TYPE_WARNING);
            $this->Messages()->add($e->getMessage(), Messages::TYPE_WARNING);
        }

        return $this->redirect()->toRoute('app');
    }

}
