<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Controller\Plugin\Messages;
use Application\Service\User as UserService;

class User extends AbstractActionController
{

    public function authenticateAction()
    {
        $viewModel = new ViewModel;

        $authService = $this->getServiceLocator()->get('AuthService');
        if ($authService->hasIdentity()) {
            return $this->redirect()->toRoute('app');
        }

        if ($this->getRequest()->isPost()) {
            $userService = $this->getServiceLocator()->get('Application\Service\User');

            $authAdapter = $authService->getAdapter();
            $authAdapter->setIdentity($this->params()->fromPost('username'));
            $authAdapter->setCredential($userService->encodeCredential($this->params()->fromPost('credencial')));

            $result = $authService->authenticate();
            if ($result->isValid()) {
                $dbAdapter = $this->getServiceLocator()->get('dbAdapter');
                $usuarioTable = new \Application\Model\UsuarioTable($dbAdapter);
                $usuarioEntity = $usuarioTable->findByEmail($authService->getIdentity());

                $authService->getStorage()->write($usuarioEntity);
                $this->getServiceLocator()->get('LogService')->log('LOGIN');

                if (false != ($redirect = $this->params()->fromPost('redirect', false))) {
                    return $this->redirect()->toUrl($redirect);
                }

                return $this->redirect()->toRoute('app');
            } else {
                $this->Messages()->add($this->translate('Usuário ou senha inválidos'), Messages::TYPE_DANGER);
            }
        }

        $params = array_merge($this->params()->fromPost(), $this->params()->fromQuery());
        $viewModel->setVariables($params, false);

        return $viewModel;
    }

    public function logoutAction()
    {
        $authService = $this->getServiceLocator()->get('AuthService');

        if ($authService->hasIdentity()) {
            $authService->clearIdentity();
        }

        return $this->redirect()->toRoute('app');
    }

    public function createPasswordAction()
    {
        $viewModel = new ViewModel;
        $token = $this->params('token');

        if ($this->getRequest()->isPost()) {
            $username = $this->params()->fromPost('username');
            $tokenService = $this->getServiceLocator()->get('TokenService');

            if ($tokenService->isValid(UserService::SERVICE_REGISTER_PASSWORD_TOKEN, $username, $token)) {
                $dbAdapter = $this->getServiceLocator()->get('dbAdapter');
                $userTable = new \Application\Model\UsuarioTable($dbAdapter);
                $userModel = $userTable->findByEmail($username);

                $credential = $this->params()->fromPost('credential');
                $userService = $this->getServiceLocator()->get('Application\Service\User');
                $userService->resetPassword($userModel, $credential);
                $tokenService->invalidate(UserService::SERVICE_REGISTER_PASSWORD_TOKEN, $token);

                $this->Messages()->add($this->translate('Senha definida com sucesso'), Messages::TYPE_SUCCESS);
                return $this->redirect()->toRoute('app/user/authenticate');
            } else {
                $this->Messages()->add($this->translate('Houve um erro com a combinação E-mail/Token. Por favor tente novamente'), Messages::TYPE_DANGER);
            }
        }

        $viewModel->setVariable('token', $token);
        return $viewModel;
    }

    public function forgotPasswordAction()
    {
        $viewModel = new ViewModel;
        $email = $this->params()->fromPost('username', false);

        if ($email) {
            $dbAdapter = $this->getServiceLocator()->get('dbAdapter');
            $userTable = new \Application\Model\UsuarioTable($dbAdapter);
            $userModel = $userTable->findByEmail($email);

            if ($userModel) {
                $userService = $this->getServiceLocator()->get('Application\Service\User');
                $userService->forgotPassword($userModel);

                $message = $this->translate('Enviamos um e-mail para <strong>%s</strong> contendo as informações necessárias para recuperar a senha de acesso');
                $this->Messages()->add(sprintf($message, $userModel->getEmail()), Messages::TYPE_SUCCESS);
                return $this->redirect()->toRoute('app/user/authenticate');
            } else {
                $this->Messages()->add($this->translate('Usuário não encontrado'), Messages::TYPE_DANGER);
            }
        }

        $viewModel->setVariable('username', $email);
        return $viewModel;
    }

    public function resetPasswordAction()
    {
        $viewModel = new ViewModel;
        $token = $this->params('token');

        if ($this->getRequest()->isPost()) {
            $username = $this->params()->fromPost('username');
            $tokenService = $this->getServiceLocator()->get('TokenService');

            if ($tokenService->isValid(UserService::SERVICE_FORGOT_PASSWORD_TOKEN, $username, $token)) {
                $dbAdapter = $this->getServiceLocator()->get('dbAdapter');
                $userTable = new \Application\Model\UsuarioTable($dbAdapter);
                $userModel = $userTable->findByEmail($username);

                $credential = $this->params()->fromPost('newCredential');
                $userService = $this->getServiceLocator()->get('Application\Service\User');
                $userService->resetPassword($userModel, $credential);
                $tokenService->invalidate(UserService::SERVICE_FORGOT_PASSWORD_TOKEN, $token);

                $this->Messages()->add($this->translate('Senha redefinida com sucesso'), Messages::TYPE_SUCCESS);
                return $this->redirect()->toRoute('app/user/authenticate');
            } else {
                $this->Messages()->add($this->translate('Houve um erro com a combinação E-mail/Token. Por favor tente novamente'), Messages::TYPE_DANGER);
            }
        }

        $viewModel->setVariable('token', $token);
        return $viewModel;
    }

    public function changePasswordAction()
    {
        $viewModel = new ViewModel;

        if ($this->getRequest()->isPost()) {
            $inputFilter = new \Application\InputFilter\ChangePassword($this->getServiceLocator()->get('dbAdapter'));
            $inputFilter->setData($this->params()->fromPost());

            if ($inputFilter->isValid()) {
                $authService = $this->getServiceLocator()->get('AuthService');
                $userService = $this->getServiceLocator()->get('Application\Service\User');
                $usuarioEntity = clone $authService->getIdentity();

                $currentCredential = $userService->encodeCredential($inputFilter->getValue('currentCredential'));
                if ($usuarioEntity->getCredencial() != $currentCredential) {
                    $this->Messages()->add($this->translate('A senha atual informada não confere'), Messages::TYPE_DANGER);
                } else {
                    try {
                        $userService->changePassword($usuarioEntity, $inputFilter->getValue('newCredential'));
                        $authService->getStorage()->write($usuarioEntity);

                        $this->Messages()->add($this->translate('Sua senha foi alterada com sucesso'), Messages::TYPE_SUCCESS);
                        return $this->redirect()->toRoute('app');
                    } catch (\Exception $e) {
                        $this->getServiceLocator()->get('Log')->logException($e);
                        $this->Messages()->add($this->translate('Não foi possível alterar sua senha. Por favor tente novamente mais tarde!'), Messages::TYPE_DANGER);
                    }
                }
            } else {
                $this->Messages()->add($this->translate('Por favor verifique os campos marcados em vermelho'), Messages::TYPE_WARNING);
                $viewModel->setVariable('errors', $inputFilter);
            }
        }

        return $viewModel;
    }

    public function accountAction()
    {
        $viewModel = new ViewModel;
        $authService = $this->getServiceLocator()->get('AuthService');
        $config = $this->getServiceLocator()->get('Application')->getConfig();
        $userEntity = $authService->getIdentity();

        if (file_exists("{$config['paths']['user_photo']['path']}/{$userEntity->getId()}.jpg")) {
            $photoPath = "{$config['paths']['user_photo']['base']}/{$userEntity->getId()}.jpg";
        } else {
            $photoPath = "{$config['paths']['user_photo']['base']}/no-user.jpg";
        }

        if ($this->getRequest()->isPost()) {
            $userEntity->setApelido($this->params()->fromPost('apelido'));

            try {
                $userService = $this->getServiceLocator()->get('Application\Service\User');
                $userService->update($userEntity);
                $userService->receiveUploadPhoto($userEntity, 'foto');

                $authService->getStorage()->write($userEntity);

                $this->messages()->add($this->translate('Dados pessoais alterados com sucesso'), Messages::TYPE_SUCCESS);
            } catch (\Application\Service\Exception\UploadException $e) {
                $this->Messages()->add($e->getMessage(), Messages::TYPE_DANGER);
            } catch (\Exception $e) {
                $message = $this->translate('Não foi possível alterar seus dados pessoais. Por favor tente novamente.');
                $this->Messages()->add($message, Messages::TYPE_DANGER);
            }
        }
        
        $viewModel->setVariable('usuario', $userEntity);
        $viewModel->setVariable('userPhoto', $photoPath);
        return $viewModel;
    }

    public function accessDeniedAction()
    {
        $viewModel = new ViewModel;

        //Unauthorized
        $this->getResponse()->setStatusCode(\Zend\Http\Response::STATUS_CODE_401);

        return $viewModel;
    }

}
