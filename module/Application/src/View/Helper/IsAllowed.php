<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * @ViewHelper
 * 
 * @author Augusto Coelho
 */
class IsAllowed extends AbstractHelper implements ServiceLocatorAwareInterface
{

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

    /**
     * @param string $resource
     * @return boolean
     */
    public function __invoke($resource)
    {
        $serviceLocator = $this->getServiceLocator()->getServiceLocator();

        $roleName = $this->getView()->plugin('view_model')->getRoot()->getVariable('aclRole');
        return $serviceLocator->get('AclService')->isAllowed($roleName, $resource);
    }

}
