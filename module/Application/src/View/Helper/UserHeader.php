<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * @ViewHelper
 * 
 * @author Augusto Coelho
 */
class UserHeader extends AbstractHelper implements ServiceLocatorAwareInterface
{

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function __invoke()
    {
        $params = array();
        $config = $this->getServiceLocator()->getServiceLocator()->get('Application')->getConfig();

        $authService = $this->getServiceLocator()->getServiceLocator()->get('AuthService');
        if ($authService->hasIdentity()) {
            $userEntity = $authService->getIdentity();
            $params['name'] = $userEntity->getApelido();

            if (file_exists("{$config['paths']['user_photo']['path']}/{$userEntity->getId()}.jpg")) {
                $photoPath = "{$config['paths']['user_photo']['base']}/{$userEntity->getId()}.jpg";
            } else {
                $photoPath = "images/users/no-user.png";
            }
            $params['photo'] = $photoPath;
        }

        return $this->getView()->render('helper/header', $params);
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

}
