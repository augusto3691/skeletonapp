<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * @ViewHelper
 * 
 * @author Augusto Coelho
 */
class AlertClass extends AbstractHelper implements ServiceLocatorAwareInterface
{

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function __invoke($score, $rgb = false)
    {
        if (false == $rgb) {
            $alertClass = array(
                '0' => 'danger',
                '51' => 'warning',
                '80' => 'success',
            );
        } else {
            $alertClass = array(
                '0' => '#d9534f',
                '51' => '#f0ad4e',
                '80' => '#449d44',
            );
        }
        $return = null;

        foreach ($alertClass as $maxPoints => $class) {
            if ($score >= $maxPoints) {
                $return = $class;
            }
        }

        return $return;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

}
