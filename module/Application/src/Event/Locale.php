<?php

namespace Application\Event;

use Zend\Mvc\MvcEvent;

/**
 * 
 * @author Augusto Coelho
 */
class Locale
{

    protected $availableLanguages = array('pt-BR', 'en-US', 'es-ES', 'fr-FR');
    protected $defaultLanguage = 'pt-BR';

    public function discoverLocale(MvcEvent $event)
    {
        $locale = null;
        $fromRoute = false;

        //see if language could be find in url or use language from http accept
        if ($event->getRouteMatch()->getParam('locale')) {
            $locale = $event->getRouteMatch()->getParam('locale');
            $fromRoute = true;
        } else {
            if (php_sapi_name() != 'cli') {
                $headers = $event->getApplication()->getRequest()->getHeaders();
                if ($headers->has('Accept-Language')) {
                    $headerLocale = $headers->get('Accept-Language')->getPrioritized();
                    $locale = $headerLocale[0]->getLanguage();
                }
            }
        }

        if (!in_array($locale, $this->availableLanguages)) {
            $locale = $this->defaultLanguage;
        }

        return $locale;
    }

    public function route(MvcEvent $event)
    {
        $locale = $this->discoverLocale($event);

        $translator = $event->getApplication()->getServiceManager()->get('translator');
        $translator->addTranslationFile('phpArray', './vendor/zendframework/zendframework/resources/languages/pt_BR/Zend_Validate.php', 'default', 'pt-BR');
        $translator->addTranslationFile('phpArray', './vendor/zendframework/zendframework/resources/languages/en/Zend_Validate.php', 'default', 'en-US');
        $translator->addTranslationFile('phpArray', './vendor/zendframework/zendframework/resources/languages/es/Zend_Validate.php', 'default', 'es-ES');
        $translator->addTranslationFile('phpArray', './vendor/zendframework/zendframework/resources/languages/fr/Zend_Validate.php', 'default', 'fr-FR');

        \Locale::setDefault($locale);
        $translator->setLocale($locale);
        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);

        if ($event->getRouter() instanceof \Zend\Mvc\Router\Http\TranslatorAwareTreeRouteStack) {
            $event->getRouter()->setTranslator($translator);
        }
        $event->getRouter()->setDefaultParam('locale', $locale);
        $event->getViewModel()->setVariable('locale', $locale);

        return true;
    }

}
