<?php

namespace Application\Event;

use Zend\EventManager\ListenerAggregateInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Mvc\MvcEvent;
use Zend\EventManager\Event;

/**
 * @Event
 * 
 * @author Augusto Coelho
 */
class Logger implements ListenerAggregateInterface, ServiceLocatorAwareInterface
{

    protected $listeners = array();

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_DISPATCH_ERROR, array($this, 'dispatchError'));

        $config = $this->getServiceLocator()->get('Application')->getConfig();
        if (isset($config['logger']) && is_array($config['logger'])) {
            foreach ($config['logger'] as $className => $classEvents) {
                foreach ($classEvents as $classEvent => $logEventName) {
                    $this->listeners[] = $this->getServiceLocator()->get($className)->getEventManager()->attach($classEvent, array($this, 'doEvent'));
                }
            }
        }
    }

    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

    public function dispatchError(MvcEvent $event)
    {
        $exception = $event->getResult()->exception;

        if ($exception) {
            $serviceManager = $event->getApplication()->getServiceManager();
            $logService = $serviceManager->get('LogService');
            $logService->logException($exception);
        }
    }

    public function doEvent(Event $event)
    {
        $config = $this->getServiceLocator()->get('Application')->getConfig();
        $classEvent = get_class($event->getTarget());
        $eventName = $config['logger'][$classEvent][$event->getName()];

        $toArrayRecursive = function($object) use(&$toArrayRecursive) {
            if (is_object($object)) {
                $object = (array) $object;
            }

            if (is_array($object)) {
                $new = array();
                foreach ($object as $key => $val) {
                    $key = trim(str_replace('*', '', $key));
                    $new[$key] = $toArrayRecursive($val);
                }
            } else {
                $new = $object;
            }
            return $new;
        };
        $params = $toArrayRecursive($event->getParams());

        $logService = $this->getServiceLocator()->get('LogService');
        $logService->log($eventName, $params);
    }

}
