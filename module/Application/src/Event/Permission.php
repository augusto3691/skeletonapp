<?php

namespace Application\Event;

use Zend\Mvc\MvcEvent;
use Zend\Http\Response;

/**
 * @Event
 * 
 * @author Augusto Coelho
 */
class Permission
{

    protected $deniedRoute;

    public function getDeniedRoute()
    {
        return $this->deniedRoute;
    }

    public function getRole()
    {
        
    }

    public function setDeniedRoute($deniedRoute)
    {
        $this->deniedRoute = (string) $deniedRoute;
        return $this;
    }

    public function route(MvcEvent $event)
    {
        $authService = $event->getApplication()->getServiceManager()->get('AuthService');
        if (!$authService->hasIdentity()) {
            return true;
        }

        $resource = $event->getRouteMatch()->getMatchedRouteName();
        $aclService = $event->getApplication()->getServiceManager()->get('AclService');
        $aclRole = "profile-{$authService->getIdentity()->getIdPerfil()}";

        $event->getViewModel()->setVariable('acl', $aclService);
        $event->getViewModel()->setVariable('aclRole', $aclRole);

        if (!$aclService->hasResource($resource) || $aclService->isAllowed($aclRole, $resource)) {
            return true;
        }

        $url = $event->getRouter()->assemble(array(), array('name' => $this->getDeniedRoute()));

        $response = $event->getResponse() ? : new Response;
        $response->setStatusCode(Response::STATUS_CODE_302);
        $response->getHeaders()->addHeaderLine('Location', $url);

        return $response;
    }

}
