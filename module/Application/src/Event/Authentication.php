<?php

namespace Application\Event;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Zend\Http\Response;

/**
 * 
 * @author Augusto Coelho
 */
class Authentication
{

    /**
     * @var type string
     */
    protected $unauthorizedRoute;

    /**
     * @var string
     */
    protected $authenticationParameter = 'authentication';

    /**
     * @var string
     */
    protected $redirectParameter = 'redirect';

    public function setUnauthorizedRoute($route)
    {
        $this->unauthorizedRoute = $route;
        return $this;
    }

    public function getUnauthorizedRoute()
    {
        return $this->unauthorizedRoute;
    }

    public function setAuthenticationParameter($authenticationParameter)
    {
        $this->authenticationParameter = (string) $authenticationParameter;
        return $this;
    }

    public function getAuthenticationParameter()
    {
        return $this->authenticationParameter;
    }

    public function setRedirectParameter($redirectParameter)
    {
        $this->redirectParameter = $redirectParameter;
        return $this;
    }

    public function getRedirectParameter()
    {
        return $this->redirectParameter;
    }

    public function route(MvcEvent $event)
    {
        $response = $event->getResponse() ? : new Response;
        $routeMatch = $event->getRouteMatch();

        if ((!$routeMatch || !$routeMatch instanceof RouteMatch) 
            || (!$routeMatch->getParam($this->getAuthenticationParameter(), false))
            || ($routeMatch->getMatchedRouteName() == $this->getUnauthorizedRoute())
        ) {
            return true;
        }

        $authServiceName = $routeMatch->getParam($this->getAuthenticationParameter(), false);
        $authService = $event->getApplication()->getServiceManager()->get($authServiceName);
        if ($authService->hasIdentity()) {
            return true;
        }

        //unauthorized
        $response->setStatusCode(Response::STATUS_CODE_401);

        if ($this->getUnauthorizedRoute()) {
            $params = array();
            $options = array('name' => $this->getUnauthorizedRoute(), 'query' => array());
            if ($this->getRedirectParameter()) {
                $options['query'] = array($this->getRedirectParameter() => $event->getRequest()->getRequestUri());
            }
            $url = $event->getRouter()->assemble($params, $options);

            $response->setStatusCode(Response::STATUS_CODE_302);
            $response->getHeaders()->addHeaderLine('Location', $url);
        }

        return $response;
    }

}
