<?php

namespace Application\Model\Perfil;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\Sql\Select;
use Application\Stdlib\TraitCacheable;

/**
 * @TableGateway
 * 
 * @author Augusto Coelho
 */
class AclTable extends AbstractTableGateway
{

    use TraitCacheable;

    protected $table = 'tb_role_acl';

    /**
     * @param \Zend\Db\Adapter\AdapterInterface $adapter
     */
    public function __construct(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new HydratingResultSet(new AclHydrator, new Acl);
        $this->initialize();
    }

    public function insertEntity(Acl $acl)
    {
        $data = $this->getResultSetPrototype()->getHydrator()->extract($acl);
        unset($data['id_Perfil_Acl']);

        $affectedRows = parent::insert($data);
        if ($affectedRows) {
            $acl->setId($this->getLastInsertValue());

            if ($this->getCacheStorage()) {
                $this->getCacheStorage()->removeItems(array(
                    $this->buildCacheHash('fetchAll'),
                ));
            }
        }

        return (bool) $affectedRows;
    }

    public function deleteFromPerfil($idPerfil)
    {
        $affectedRows = $this->delete(array('id_Perfil' => (int) $idPerfil));

        if ($this->getCacheStorage()) {
            $this->getCacheStorage()->removeItems(array(
                $this->buildCacheHash('fetchAll'),
            ));
        }

        return $affectedRows;
    }

    /**
     * @return \Zend\Db\ResultSet\AbstractResultSet <\Application\Model\Perfil\Acl>
     */
    public function fetchAll()
    {
        $select = new Select($this->table);

        $resultSet = $this->selectWith($select);
        $resultSet->initialize($resultSet->toArray());

        return $resultSet;
    }

}
