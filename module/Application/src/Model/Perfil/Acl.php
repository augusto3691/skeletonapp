<?php

namespace Application\Model\Perfil;

/**
 * @Entity
 * 
 * @author Augusto Coelho
 */
class Acl
{

    const ACTION_ALLOW = 'allow';
    const ACTION_DENY = 'deny';

    protected $id;
    protected $idPerfil;
    protected $action;
    protected $resource;

    public function getId()
    {
        return $this->id;
    }

    public function getIdPerfil()
    {
        return $this->idPerfil;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function getResource()
    {
        return $this->resource;
    }

    public function setId($id)
    {
        $this->id = (int) $id;
        return $this;
    }

    public function setIdPerfil($idPerfil)
    {
        $this->idPerfil = (int) $idPerfil;
        return $this;
    }

    public function setAction($action)
    {
        $this->action = (string) $action;
        return $this;
    }

    public function setResource($resource)
    {
        $this->resource = (string) $resource;
        return $this;
    }

}
