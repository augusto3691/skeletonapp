<?php

namespace Application\Model\Perfil;

use Zend\Stdlib\Hydrator\AbstractHydrator;

/**
 * @Hydrator
 * 
 * @author Augusto Coelho
 */
class AclHydrator extends AbstractHydrator
{

    public function extract($object)
    {
        if (!($object instanceof Acl)) {
            throw new \Exception('Objeto não correspondente!');
        }

        $data = array(
            'id_Perfil_Acl' => $object->getId(),
            'id_Perfil' => $object->getIdPerfil(),
            'ch_Acao' => $object->getAction(),
            'ch_Recurso' => $object->getResource(),
        );

        return $data;
    }

    public function hydrate(array $data, $object)
    {
        if (!($object instanceof Acl)) {
            throw new \Exception('Objeto não correspondente!');
        }

        $object->setId(isset($data['id_Perfil_Acl']) ? $data['id_Perfil_Acl'] : null);
        $object->setIdPerfil(isset($data['id_Perfil']) ? $data['id_Perfil'] : null);
        $object->setAction(isset($data['ch_Acao']) ? $data['ch_Acao'] : null);
        $object->setResource(isset($data['ch_Recurso']) ? $data['ch_Recurso'] : null);

        return $object;
    }

}
