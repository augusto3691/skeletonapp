<?php

namespace Application\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\Sql\Select;
use Application\Stdlib\TraitCacheable;

/**
 * @TableGateway
 * 
 * @author Augusto Coelho
 */
class PerfilTable extends AbstractTableGateway
{

    use TraitCacheable;

    protected $table = 'tb_role';

    /**
     * @param \Zend\Db\Adapter\AdapterInterface $adapter
     */
    public function __construct(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new HydratingResultSet(new PerfilHydrator, new Perfil);
        $this->initialize();
    }

    /**
     * @param \Application\Model\Perfil $profile
     * @return bool
     */
    public function insertEntity(Perfil $profile)
    {
        $data = $this->getResultSetPrototype()->getHydrator()->extract($profile);
        unset($data['id_Perfil']);

        $affectedRows = parent::insert($data);
        if ($affectedRows) {
            $profile->setId($this->getLastInsertValue());

            if ($this->getCacheStorage()) {
                $this->getCacheStorage()->removeItems(array(
                    $this->buildCacheHash('fetchAll'),
                    $this->buildCacheHash('findByAtivos'),
                ));
            }
        }

        return (bool) $affectedRows;
    }

    /**
     * @param \Application\Model\Perfil $profile
     * @return bool
     */
    public function updateEntity(Perfil $profile)
    {
        $data = $this->getResultSetPrototype()->getHydrator()->extract($profile);
        unset($data['id_Perfil']);
        $where = array('id_Perfil' => $profile->getId());

        $affectedRows = parent::update($data, $where);
        if ($affectedRows) {
            if ($this->getCacheStorage()) {
                $this->getCacheStorage()->removeItems(array(
                    $this->buildCacheHash('fetchAll'),
                    $this->buildCacheHash('findByAtivos'),
                ));
            }
        }

        return (bool) $affectedRows;
    }

    /**
     * @return \Application\Model\Perfil[]
     */
    public function fetchAll()
    {
        $select = new Select($this->table);
        $select->order('ch_Perfil ASC');

        $resultSet = $this->selectWith($select);
        $resultSet->initialize($resultSet->toArray());

        return $resultSet;
    }

    /**
     * @param integer $id
     * @return \Application\Model\Perfil
     */
    public function findById($id)
    {
        $select = new Select($this->table);
        $select->where->equalTo('id_Perfil', (int) $id);

        $resultSet = $this->selectWith($select);
        $resultSet->initialize($resultSet->toArray());

        return $resultSet->current();
    }

    public function findByAtivos()
    {
        $select = new Select($this->table);
        $select->where->equalTo('flg_Inativo', false);

        $resultSet = $this->selectWith($select);
        $resultSet->initialize($resultSet->toArray());

        return $resultSet;
    }

    /**
     * @param \Application\Model\Usuario $usuario
     * @return \Zend\Db\ResultSet\AbstractResultSet <\Application\Model\Perfil>
     */
    public function findByUsuario(Usuario $usuario)
    {
        $select = new Select;
        $select->from(array('p' => $this->table))
            ->join(array('g' => 'tb_usersPerfil'), 'g.id_Perfil = p.id_Perfil', array())
            ->where
            ->equalTo('g.id_Participante', $usuario->getId())
            ->AND
            ->equalTo('p.flg_Inativo', false);

        $statement = $this->adapter->createStatement();
        $select->prepareStatement($this->adapter, $statement);
        $result = $statement->execute();

        $resultSet = $this->getResultSetPrototype();
        $resultSet->initialize($result->getResource()->fetchAll());

        return $resultSet;
    }

}
