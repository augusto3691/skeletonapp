<?php

namespace Application\Model;

use Zend\Stdlib\Hydrator\AbstractHydrator;
use DateTime;
use Application\Stdlib\DateConvert;

/**
 * @Hydrator
 * 
 * @author Augusto Coelho
 */
class UsuarioHydrator extends AbstractHydrator
{

    public function extract($object)
    {
        if (!($object instanceof Usuario)) {
            throw new \Exception('Objeto não correspondente!');
        }

        $data = array(
            'id_Participante' => (int) $object->getId(),
            'id_Perfil' => (int) $object->getIdPerfil(),
            'dt_Inclusao' => $object->getDataCadastro() instanceof DateTime ? DateConvert::toDatabase($object->getDataCadastro()) : null,
            'ch_Nome' => $object->getNome(),
            'ch_SobreNome' => $object->getSobrenome(),
            'ch_Apelido' => $object->getApelido(),
            'ch_Email' => $object->getEmail(),
            'ch_Senha' => $object->getCredencial(),
            'flg_Inativo' => (int) !$object->getAtivo(),
            'ch_Perfil' => $object->getLabelPerfil()
        );

        $data['flg_CadastroCompleto'] = '1';

        return $data;
    }

    public function hydrate(array $data, $object)
    {
        if (!($object instanceof Usuario)) {
            throw new \Exception('Objeto não correspondente!');
        }
        $object->setId(isset($data['id_Participante']) ? $data['id_Participante'] : null);
        $object->setIdPerfil(isset($data['id_Perfil']) ? $data['id_Perfil'] : null);
        $object->setLabelPerfil(isset($data['ch_Perfil']) ? $data['ch_Perfil'] : null);
        if (isset($data['dt_Inclusao'])) {
            $object->setDataCadastro(DateConvert::fromDatabase($data['dt_Inclusao']));
        }
        $object->setNome(isset($data['ch_Nome']) ? $data['ch_Nome'] : null);
        $object->setSobrenome(isset($data['ch_SobreNome']) ? $data['ch_SobreNome'] : null);
        $object->setApelido(isset($data['ch_Apelido']) ? $data['ch_Apelido'] : null);
        $object->setEmail(isset($data['ch_Email']) ? $data['ch_Email'] : null);
        $object->setCredencial(isset($data['ch_Senha']) ? $data['ch_Senha'] : null);
        $object->setAtivo(isset($data['flg_Inativo']) ? !((bool) $data['flg_Inativo']) : null);
        return $object;
    }

}
