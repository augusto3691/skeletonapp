<?php

namespace Application\Model;

use Zend\Stdlib\Hydrator\AbstractHydrator;
use DateTime;
use Application\Stdlib\DateConvert;

/**
 * @Hydrator
 * 
 * @author Augusto Coelho
 */
class PerfilHydrator extends AbstractHydrator
{

    public function extract($object)
    {
        if (!($object instanceof Perfil)) {
            throw new \Exception('Objeto não correspondente!');
        }

        $data = array(
            'id_Perfil' => $object->getId(),
            'dt_Inclusao' => $object->getDataCadastro() instanceof DateTime ? DateConvert::toDatabase($object->getDataCadastro()) : null,
            'ch_Perfil' => $object->getNome(),
            'ch_Descricao' => $object->getDescricao(),
            'flg_Inativo' => (int) !$object->getAtivo(),
        );

        return $data;
    }

    public function hydrate(array $data, $object)
    {
        if (!($object instanceof Perfil)) {
            throw new \Exception('Objeto não correspondente!');
        }

        $object->setId(isset($data['id_Perfil']) ? $data['id_Perfil'] : null);
        $object->setDataCadastro(DateConvert::fromDatabase($data['dt_Inclusao']));
        $object->setNome(isset($data['ch_Perfil']) ? $data['ch_Perfil'] : null);
        $object->setDescricao(isset($data['ch_Descricao']) ? $data['ch_Descricao'] : null);
        $object->setAtivo(isset($data['flg_Inativo']) ? !((bool) $data['flg_Inativo']) : null);

        return $object;
    }

}
