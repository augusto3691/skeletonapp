<?php

namespace Application\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\Sql\Select;
use Application\Stdlib\TraitCacheable;

/**
 * @TableGateway
 * 
 * @author Augusto Coelho
 */
class UsuarioTable extends AbstractTableGateway
{

    use TraitCacheable;

    protected $table = 'tb_users';

    /**
     * @param \Zend\Db\Adapter\AdapterInterface $adapter
     */
    public function __construct(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new HydratingResultSet(new UsuarioHydrator, new Usuario);
        $this->initialize();
    }

    /**
     * @param \Application\Model\Usuario $user
     * @return boolean
     */
    public function insertEntity(Usuario $user)
    {
        $data = $this->getResultSetPrototype()->getHydrator()->extract($user);
        unset($data['id_Participante']);
        unset($data['ch_Perfil']);

        $affectedRows = parent::insert($data);
        if ($affectedRows) {
            $user->setId($this->getLastInsertValue());
        }

        return (bool) $affectedRows;
    }

    /**
     * @param \Application\Model\Usuario $user
     * @return boolean
     */
    public function updateEntity(Usuario $user)
    {
        $data = $this->getResultSetPrototype()->getHydrator()->extract($user);
        unset($data['id_Participante']);
        unset($data['ch_Perfil']);
        $where = array('id_Participante' => $user->getId());

        $affectedRows = parent::update($data, $where);
        if ($affectedRows) {
            if ($this->getCacheStorage()) {
                $this->getCacheStorage()->removeItems(array(
                    $this->buildCacheHash('findById', array($user->getId())),
                    $this->buildCacheHash('findByPerfil', array($user->getId())),
                    $this->buildCacheHash('findByEmail', array($user->getId())),
                ));
            }
        }

        return (bool) $affectedRows;
    }

    /**
     * @param integer $id
     * @return \Application\Model\Usuario
     */
    public function findById($id)
    {
        $select = new Select($this->table);
        $select->where->equalTo('id_Participante', (int) $id);

        $resultSet = $this->selectWith($select);
        $resultSet->initialize($resultSet->toArray());

        return $resultSet->current();
    }

    /**
     * @param integer $id
     * @return \Application\Model\Usuario
     */
    public function fetchAll()
    {
        $select = new Select(array('u' => $this->table));
        $select->join(array('r' => 'tb_role'), 'u.id_Perfil = r.id_Perfil', array('ch_Perfil'));
        $select->order('ch_Nome ASC');

        $resultSet = $this->selectWith($select);
        $resultSet->initialize($resultSet->toArray());

        return $resultSet;
    }

    /**
     * @param integer $perfil
     * @return \Zend\Db\ResultSet\AbstractResultSet <\Application\Model\Usuario>
     */
    public function findByPerfil($perfil)
    {
        $select = new Select($this->table);
        $select->where->equalTo('id_Perfil', (int) $perfil);

        $resultSet = $this->selectWith($select);
        $resultSet->initialize($resultSet->toArray());

        return $resultSet;
    }

    /**
     * 
     * @param type $idPerfil
     * @param type $nome
     * @return type
     */
    public function findByPerfilAndNome($idPerfil, $nome)
    {
        $select = new Select($this->table);

        if ($idPerfil) {
            $select->where->equalTo('id_Perfil', (int) $idPerfil);
        }

        if ($nome) {
            $select->where
                ->nest()
                ->like("ch_Nome", "%{$nome}%")->OR
                ->like("ch_Sobrenome", "%{$nome}%")
                ->unnest();
        }

        $select->order('ch_Nome ASC')->order('ch_Sobrenome ASC');

        $statement = $this->adapter->createStatement();
        $select->prepareStatement($this->adapter, $statement);
        $result = $statement->execute();

        $resultSet = $this->getResultSetPrototype();
        $resultSet->initialize($result);

        return $resultSet;
    }

    /**
     * @param string $email
     * @return \Application\Model\Usuario
     */
    public function findByEmail($email)
    {
        $select = new Select(array('u' => $this->table));
        $select->join(array('r' => 'tb_role'), 'u.id_Perfil = r.id_Perfil', array('ch_Perfil'));
        $select->where->equalTo('ch_Email', (string) $email);

        $resultSet = $this->selectWith($select);
        $resultSet->initialize($resultSet->toArray());

        return $resultSet->current();
    }

    /**
     * @param string $resource
     * @return \Zend\Db\ResultSet\AbstractResultSet <\Application\Model\Usuario>
     */
    public function fetchByPermission($resource)
    {
        $select = new Select;
        $select->from(array('u' => $this->table))
            ->join(array('r' => 'tb_role_acl'), 'r.id_Perfil = u.id_Perfil', array())
            ->where
            ->equalTo('r.ch_Recurso', $resource)->AND
            ->equalTo('u.flg_Inativo', 0);
        $select->order('ch_Nome ASC');

        $statement = $this->adapter->createStatement();
        $select->prepareStatement($this->adapter, $statement);
        $result = $statement->execute();

        $resultSet = clone $this->getResultSetPrototype();
        $resultSet->initialize($result->getResource()->fetchAll(\PDO::FETCH_ASSOC));

        return $resultSet;
    }

}
