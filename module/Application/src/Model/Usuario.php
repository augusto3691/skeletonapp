<?php

namespace Application\Model;

use DateTime;

/**
 * @Entity
 * 
 * @author Augusto Coelho
 */
class Usuario
{

    protected $id;
    protected $idPerfil;
    protected $labelPerfil;

    /**
     * @var \DateTime
     */
    protected $dataCadastro;
    protected $nome;
    protected $sobrenome;
    protected $apelido;
    protected $email;
    protected $credencial;

    /**
     * @var bool
     */
    protected $ativo = true;

    public function getId()
    {
        return $this->id;
    }

    public function getIdPerfil()
    {
        return $this->idPerfil;
    }

    public function getLabelPerfil()
    {
        return $this->labelPerfil;
    }

    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function getSobrenome()
    {
        return $this->sobrenome;
    }

    public function getApelido()
    {
        return $this->apelido;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getCredencial()
    {
        return $this->credencial;
    }

    public function getAtivo()
    {
        return $this->ativo;
    }

    public function setId($id)
    {
        $this->id = (int) $id;
        return $this;
    }

    public function setIdPerfil($idPerfil)
    {
        $this->idPerfil = (int) $idPerfil;
        return $this;
    }

    public function setLabelPerfil($labelPerfil)
    {
        $this->labelPerfil = $labelPerfil;
        return $this;
    }

    public function setDataCadastro(DateTime $dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
        return $this;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    public function setSobrenome($sobrenome)
    {
        $this->sobrenome = (string) $sobrenome;
        return $this;
    }

    public function setApelido($apelido)
    {
        $this->apelido = $apelido;
        return $this;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function setCredencial($credencial)
    {
        $this->credencial = $credencial;
        return $this;
    }

    public function setAtivo($ativo)
    {
        $this->ativo = (bool) $ativo;
        return $this;
    }

}
