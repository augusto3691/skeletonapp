<?php

namespace Application\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Sql\Select;

/**
 * @TableGateway
 * 
 * @author Augusto Coelho
 */
class TokenTable extends AbstractTableGateway
{

    protected $table = 'tb_Token';

    /**
     * @param \Zend\Db\Adapter\AdapterInterface $adapter
     */
    public function __construct(AdapterInterface $adapter)
    {
        $this->adapter = $adapter;
        $this->initialize();
    }

    public function findValidToken($service, $keyword, $token)
    {
        $now = new \DateTime;

        $select = new Select($this->table);
        $select->where
            ->equalTo('ch_Servico', $service)
            ->AND
            ->equalTo('ch_Chave', $keyword)
            ->AND
            ->equalTo('ch_Token', $token)
            ->AND
            ->greaterThanOrEqualTo('dt_Expiracao', $now->format('Y-m-d H:i:s'));

        $resultSet = $this->selectWith($select);
        return $resultSet->current();
    }

}
