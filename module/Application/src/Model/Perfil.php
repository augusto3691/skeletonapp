<?php

namespace Application\Model;

use DateTime;

/**
 * @Entity
 * 
 * @author Augusto Coelho<eduschmidt10@gmailcom>
 */
class Perfil
{

    const TIPO_TUTOR = 2;
    const TIPO_AGENTE = 3;

    protected $id;

    /**
     * @var DateTime
     */
    protected $dataCadastro;
    protected $nome;
    protected $descricao;
    protected $ativo = true;

    public function getId()
    {
        return $this->id;
    }

    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function getDescricao()
    {
        return $this->descricao;
    }

    public function getAtivo()
    {
        return $this->ativo;
    }

    public function setId($id)
    {
        $this->id = (int) $id;
        return $this;
    }

    public function setDataCadastro(DateTime $dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
        return $this;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
        return $this;
    }

    public function setAtivo($ativo)
    {
        $this->ativo = (bool) $ativo;
        return $this;
    }

}
