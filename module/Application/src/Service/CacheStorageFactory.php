<?php

namespace Application\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Cache\StorageFactory;

/**
 * @Factory
 * 
 * @author Augusto Coelho
 */
class CacheStorageFactory implements FactoryInterface
{

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;

        $cacheStorage = StorageFactory::factory(array(
                'adapter' => 'filesystem',
                'plugins' => array(
                    'exception_handler' => array(
                        'throw_exceptions' => false,
                    ),
                    'serializer',
                ),
                'options' => array(
                    'ttl' => (60 * 60 * 3), //3 horas
                    'cache_dir' => APPLICATION_PATH . '/data/cache',
                ),
        ));

        return $cacheStorage;
    }

}
