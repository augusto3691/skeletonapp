<?php

namespace Application\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Role\GenericRole;
use Zend\Permissions\Acl\Resource\GenericResource;
use Application\Model\Perfil\Acl as AclEntity;

/**
 * @Factory
 * 
 * @author Augusto Coelho
 */
class AclFactory implements FactoryInterface
{

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function loadRoles(Acl &$acl)
    {
        $dbAdapter = $this->serviceLocator->get('dbAdapter');
        $cacheStorage = $this->serviceLocator->get('CacheStorage');

        $perfilTable = new \Application\Model\PerfilTable($dbAdapter);
        $perfilTable->setCacheStorage($cacheStorage);
        $perfilList = $perfilTable->findByAtivosFromCache();

        foreach ($perfilList as $perfilEntity) {
            $roleName = "profile-{$perfilEntity->getId()}";
            if (!$acl->hasRole($roleName)) {
                $acl->addRole(new GenericRole($roleName));
            }
        }
    }

    public function loadResources(Acl &$acl)
    {
        $loadResources = function($resourceParent, array $config) use (&$loadResources, &$acl) {
            foreach ($config as $resourceName => $routeConfig) {
                $resourceId = "{$resourceParent}{$resourceName}";

                if (isset($routeConfig['options']['defaults']['permission']) && ((bool) $routeConfig['options']['defaults']['permission']) == true) {
                    $acl->addResource(new GenericResource($resourceId));
                }

                if (isset($routeConfig['child_routes'])) {
                    $loadResources("{$resourceId}/", $routeConfig['child_routes']);
                }
            }
        };

        $config = $this->serviceLocator->get('Application')->getConfig();
        $loadResources('', $config['router']['routes']);
    }

    public function loadRules(Acl &$acl)
    {
        $dbAdapter = $this->serviceLocator->get('dbAdapter');
        $cacheStorage = $this->serviceLocator->get('CacheStorage');

        $perfilAclTable = new \Application\Model\Perfil\AclTable($dbAdapter);
        $perfilAclTable->setCacheStorage($cacheStorage);
        $aclList = $perfilAclTable->fetchAllFromCache();

        foreach ($aclList as $aclEntity) {
            if (!$acl->hasResource($aclEntity->getResource())) {
                continue;
            }

            $roleName = "profile-{$aclEntity->getIdPerfil()}";

            if ($aclEntity->getAction() == AclEntity::ACTION_ALLOW) {
                $acl->allow($roleName, $acl->getResource($aclEntity->getResource()));
            } elseif ($aclEntity->getAction() == AclEntity::ACTION_DENY) {
                $acl->deny($roleName, $acl->getResource($aclEntity->getResource()));
            } else {
                throw new \Exception("No valid permission defined: {$aclEntity->getAction()}");
            }
        }
    }

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;

        $acl = new Acl;
        $this->loadRoles($acl);
        $this->loadResources($acl);
        $this->loadRules($acl);

        return $acl;
    }

}
