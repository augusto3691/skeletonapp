<?php

namespace Application\Service;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;
use Application\Model\Perfil as ProfileModel;

/**
 * @Service
 * 
 * @author Augusto Coelho
 */
class Profile implements ServiceLocatorAwareInterface, EventManagerAwareInterface
{

    const EVENT_REGISTER_PRE = 'register.pre';
    const EVENT_REGISTER_POS = 'register.pos';
    const EVENT_UPDATE_PRE = 'update.pre';
    const EVENT_UPDATE_POS = 'update.pos';
    const EVENT_SET_PERMISSIONS_PRE = 'setpermissions.pre';
    const EVENT_SET_PERMISSIONS_POS = 'setpermissions.pos';

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * @var \Zend\EventManager\EventManagerInterface
     */
    protected $eventManager;

    /**
     * Register a new profile
     * 
     * @param \Application\Model\Perfil $profile
     * @return boolean
     */
    public function register(ProfileModel $profile)
    {
        $this->getEventManager()->trigger(self::EVENT_REGISTER_PRE, $this, array('profile' => $profile));

        $profile->setDataCadastro(new \DateTime);

        $dbAdapter = $this->getServiceLocator()->get('dbAdapter');
        $cacheStorage = $this->getServiceLocator()->get('CacheStorage');
        $profileTable = new \Application\Model\PerfilTable($dbAdapter);
        $profileTable->setCacheStorage($cacheStorage);

        $affecteds = $profileTable->insertEntity($profile);

        $this->getEventManager()->trigger(self::EVENT_REGISTER_POS, $this, array('profile' => $profile));
        return $affecteds;
    }

    /**
     * Updates the profile data
     * 
     * @param \Application\Model\Perfil $profile
     * @return boolean
     */
    public function update(ProfileModel $profile)
    {
        $this->getEventManager()->trigger(self::EVENT_UPDATE_PRE, $this, array('profile' => $profile));

        $dbAdapter = $this->getServiceLocator()->get('dbAdapter');
        $cacheStorage = $this->getServiceLocator()->get('CacheStorage');

        $profileTable = new \Application\Model\PerfilTable($dbAdapter);
        $profileTable->setCacheStorage($cacheStorage);

        $affecteds = $profileTable->updateEntity($profile);

        $this->getEventManager()->trigger(self::EVENT_UPDATE_POS, $this, array('profile' => $profile));
        return $affecteds;
    }

    /**
     * Set permissions for profile
     * 
     * @param \Application\Model\Perfil $profile
     * @param \Application\Model\Perfil\Acl[] $aclList
     * @return boolean
     * @throws \Application\Service\Exception
     */
    public function setPermissions(ProfileModel $profile, array $aclList)
    {
        $this->getEventManager()->trigger(self::EVENT_SET_PERMISSIONS_PRE, $this, array('profile' => $profile, 'aclList' => $aclList));

        $dbAdapter = $this->getServiceLocator()->get('dbAdapter');
        $cacheStorage = $this->getServiceLocator()->get('CacheStorage');

        $profileAclTable = new \Application\Model\Perfil\AclTable($dbAdapter);
        $profileAclTable->setCacheStorage($cacheStorage);

        $connection = $dbAdapter->getDriver()->getConnection();
        try {
            $connection->beginTransaction();

            $profileAclTable->deleteFromPerfil($profile->getId());
            foreach ($aclList as $aclEntity) {
                $aclEntity->setIdPerfil($profile->getId());
                $profileAclTable->insertEntity($aclEntity);
            }

            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollback();
            throw $e;
        }

        $this->getEventManager()->trigger(self::EVENT_SET_PERMISSIONS_POS, $this, array('profile' => $profile, 'aclList' => $aclList));
        return true;
    }

    public function getEventManager()
    {
        return $this->eventManager;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function setEventManager(EventManagerInterface $eventManager)
    {
        $this->eventManager = $eventManager;
        return $this;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

}
