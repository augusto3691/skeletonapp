<?php

namespace Application\Service;

use Application\Stdlib\ServiceProvider;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp;
use Zend\Mail\Transport\SmtpOptions;

/**
 * @Service
 * 
 * @author Augusto Coelho
 */
class Mail extends ServiceProvider
{

    const EVENT_SEND_PRE = 'send.pre';
    const EVENT_SEND_POS = 'send.pos';

    public function enviar(Message $message, $server = 'default')
    {
        $this->getEventManager()->trigger(self::EVENT_SEND_PRE, $this, array('message' => $message));

        $config = $this->getServiceManager()->get('Application')->getConfig();
        $mailConfig = $config['mail'][$server];

        $message->setFrom($mailConfig['username'], $mailConfig['sender']);
        $message->setReplyTo($mailConfig['replyTo']);
        $options = new SmtpOptions(array(
            'host' => $mailConfig['server'],
            'port' => $mailConfig['port'],
            'connection_class' => 'login',
            'connection_config' => array(
                'username' => $mailConfig['username'],
                'password' => $mailConfig['password']
            )
        ));

        $transport = new Smtp($options);
        $result = $transport->send($message);

        $this->getEventManager()->trigger(self::EVENT_SEND_POS, $this, array('message' => $message));
        return $result;
    }

}
