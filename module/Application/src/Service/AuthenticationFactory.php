<?php

namespace Application\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Authentication\Adapter\DbTable as DbTableAdapter;
use Zend\Session\SessionManager;
use Zend\Authentication\Storage\Session as SessionStorage;
use Zend\Authentication\AuthenticationService;

/**
 * @Factory
 * 
 * @author Augusto Coelho
 */
class AuthenticationFactory implements FactoryInterface
{

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;

        $authAdapter = new DbTableAdapter($serviceLocator->get('dbAdapter'));
        $authAdapter->setTableName('tb_users')
            ->setIdentityColumn('ch_Email')
            ->setCredentialColumn('ch_Senha')
            ->setCredentialTreatment('? AND flg_Inativo = 0');

        $sessionManager = new SessionManager;
        $authStorage = new SessionStorage('Auth_', null, $sessionManager);
        
        return new AuthenticationService($authStorage, $authAdapter);
    }

}
