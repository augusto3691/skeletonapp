<?php

namespace Application\Service;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;
use Application\Model\Usuario as UsuarioModel;
use Zend\Db\Sql\Sql;

/**
 * @Service
 * 
 * @author Augusto Coelho
 */
class User implements ServiceLocatorAwareInterface, EventManagerAwareInterface
{

    const SERVICE_REGISTER_PASSWORD_TOKEN = 'REGISTERPASSWD';
    const SERVICE_FORGOT_PASSWORD_TOKEN = 'FPASSWD';
    const EVENT_REGISTER_PRE = 'register.pre';
    const EVENT_REGISTER_POS = 'register.pos';
    const EVENT_UPDATE_PRE = 'update.pre';
    const EVENT_UPDATE_POS = 'update.pos';
    const EVENT_CHANGE_PASSWORD = 'change-password';
    const EVENT_FORGOT_PASSWORD = 'forgot-password';
    const EVENT_RESET_PASSWORD = 'forgot-password';

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * @var \Zend\EventManager\EventManagerInterface
     */
    protected $eventManager;

    /**
     * Register a new user
     * 
     * @param \Application\Model\Usuario $user
     * @return boolean
     * @throws \Exception
     */
    public function register(UsuarioModel $user)
    {
        $this->getEventManager()->trigger(self::EVENT_REGISTER_PRE, $this, array('user' => $user));

        $user->setDataCadastro(new \DateTime);

        $dbAdapter = $this->getServiceLocator()->get('dbAdapter');
        $connection = $dbAdapter->getDriver()->getConnection();
        $connection->beginTransaction();
        try {
            $userTable = new \Application\Model\UsuarioTable($dbAdapter);
            $userTable->insertEntity($user);

            //token for create password
            $tokenExpires = new \DateTime;
            $tokenExpires->add(new \DateInterval("P90D"));

            $tokenService = $this->getServiceLocator()->get('TokenService');
            $token = $tokenService->generate(self::SERVICE_REGISTER_PASSWORD_TOKEN, $user->getEmail(), $tokenExpires);

            //welcome mail
            $viewMail = new \Zend\View\Model\ViewModel;
            $viewMail->setTemplate('email/user/register');
            $viewMail->setVariable('name', $user->getNome());
            $viewMail->setVariable('token', $token);
            $viewMail->setVariable('validity', $tokenExpires);

            $htmlPart = new \Zend\Mime\Part($this->getServiceLocator()->get('viewmanager')->getRenderer()->render($viewMail));
            $htmlPart->type = \Zend\Mime\Mime::TYPE_HTML;
            $htmlPart->charset = 'UTF-8';

            $mailBody = new \Zend\Mime\Message;
            $mailBody->addPart($htmlPart);

            $mailMessage = new \Zend\Mail\Message;
            $mailMessage->setEncoding('UTF-8');
            $mailMessage->addTo($user->getEmail(), $user->getNome());
            $mailMessage->setSubject(APPLICATION_NAME . " - {$this->getServiceLocator()->get('translator')->translate('Bem vindo')}");
            $mailMessage->setBody($mailBody);

            $mailService = $this->getServiceLocator()->get('MailService');
            $mailService->enviar($mailMessage);

            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollback();
            $this->getServiceLocator()->get('LogService')->logException($e);
            throw $e;
        }

        $this->getEventManager()->trigger(self::EVENT_REGISTER_POS, $this, array('user' => $user));
        return true;
    }

    /**
     * Updates the user data
     * 
     * @param \Application\Model\Usuario $user
     * @return boolean
     */
    public function update(UsuarioModel $user)
    {
        $this->getEventManager()->trigger(self::EVENT_UPDATE_PRE, $this, array('user' => $user));

        $dbAdapter = $this->getServiceLocator()->get('dbAdapter');
        $cacheStorage = $this->getServiceLocator()->get('CacheStorage');

        $userTable = new \Application\Model\UsuarioTable($dbAdapter);
        $userTable->setCacheStorage($cacheStorage);

        $affecteds = $userTable->updateEntity($user);

        $this->getEventManager()->trigger(self::EVENT_UPDATE_POS, $this, array('user' => $user));
        return $affecteds;
    }

    /**
     * @param \Application\Model\Usuario $user
     * @param string $fieldName
     * @return boolean
     * @throws Exception\UploadException
     */
    public function receiveUploadPhoto(UsuarioModel $user, $fieldName)
    {
        $transferAdapter = new \Zend\File\Transfer\Adapter\Http;
        if ($transferAdapter->isUploaded($fieldName)) {
            $config = $this->getServiceLocator()->get('Application')->getConfig();

            $transferAdapter->addValidator(new \Zend\Validator\File\Extension(array(
                'case' => false,
                'extension' => 'jpg',
            )));

            $transferAdapter->addFilter(new \Zend\Filter\File\Rename(array(
                'target' => "{$user->getId()}.jpg",
                'overwrite' => true,
            )));
            $transferAdapter->setDestination($config['paths']['user_photo']['path']);

            if (!$transferAdapter->receive()) {
                throw new Exception\UploadException(current($transferAdapter->getMessages()));
            }
        }

        return true;
    }

    /**
     * @param \Application\Model\Usuario $user
     * @param string $password Not encrypted
     */
    public function changePassword(UsuarioModel $user, $password)
    {
        $encryptedPassword = $this->encodeCredential($password);
        $user->setCredencial($encryptedPassword);

        $dbAdapter = $this->getServiceLocator()->get('dbAdapter');
        $usuarioTable = new \Application\Model\UsuarioTable($dbAdapter);
        $usuarioTable->updateEntity($user);

        $this->getServiceLocator()->get('LogService')->log('CHANGE_PASSWORD');
        $this->getEventManager()->trigger(self::EVENT_CHANGE_PASSWORD, $this, array('user' => $user, 'password' => $password));
        return true;
    }

    public function forgotPassword(UsuarioModel $user)
    {
        $expires = new \DateTime;
        $expires->add(new \DateInterval("P5D"));

        $tokenService = $this->getServiceLocator()->get('TokenService');
        $token = $tokenService->generate(self::SERVICE_FORGOT_PASSWORD_TOKEN, $user->getEmail(), $expires);

        //trigger email
        $viewMail = new \Zend\View\Model\ViewModel;
        $viewMail->setTemplate('email/user/forgot-password');
        $viewMail->setVariable('name', $user->getNome());
        $viewMail->setVariable('token', $token);
        $viewMail->setVariable('validity', $expires);

        $htmlPart = new \Zend\Mime\Part($this->getServiceLocator()->get('viewmanager')->getRenderer()->render($viewMail));
        $htmlPart->type = \Zend\Mime\Mime::TYPE_HTML;
        $htmlPart->charset = 'UTF-8';

        $mailBody = new \Zend\Mime\Message;
        $mailBody->addPart($htmlPart);

        $mailMessage = new \Zend\Mail\Message;
        $mailMessage->setEncoding('UTF-8');
        $mailMessage->addTo($user->getEmail(), $user->getNome());
        $mailMessage->setSubject(APPLICATION_NAME . " - {$this->getServiceLocator()->get('translator')->translate('Esqueci minha senha')}");
        $mailMessage->setBody($mailBody);

        $mailService = $this->getServiceLocator()->get('MailService');
        $mailService->enviar($mailMessage);

        $this->getEventManager()->trigger(self::EVENT_FORGOT_PASSWORD, $this, array('user' => $user, 'token' => $token));
        return true;
    }

    public function resetPassword(UsuarioModel $user, $credential)
    {
        $userModel = clone $user;
        $userModel->setCredencial($this->encodeCredential($credential));

        $dbAdapter = $this->getServiceLocator()->get('dbAdapter');
        $userTable = new \Application\Model\UsuarioTable($dbAdapter);
        $userTable->updateEntity($userModel);

        $user = $userModel;
        $this->getEventManager()->trigger(self::EVENT_RESET_PASSWORD, $this, array('user' => $user, 'credential' => $credential));
        return true;
    }

    /**
     * @param string $credential
     * @return string
     */
    public function encodeCredential($credential)
    {
        $credential = strtoupper(md5($credential));
        $mhash = pack('H*', $credential);
        return base64_encode($mhash);
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function getEventManager()
    {
        return $this->eventManager;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

    public function setEventManager(EventManagerInterface $eventManager)
    {
        $this->eventManager = $eventManager;
        return $this;
    }

}
