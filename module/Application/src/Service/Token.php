<?php

namespace Application\Service;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Application\Stdlib\DateConvert;

/**
 * 
 * @author Augusto Coelho
 */
class Token implements ServiceLocatorAwareInterface
{

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * Create a new Token
     * 
     * @param string $service
     * @param string $keyword
     * @param \DateTime $expires
     * @return string Generated token
     */
    public function generate($service, $keyword, \DateTime $expires = null)
    {
        $characters = '0123456789abcdefghijlmnopqrstuvxyzwABCDEFGHIJLMNOPQRSTUVXYZW';
        $tokenString = '';
        while (strlen($tokenString) <= 15) {
            $tokenString .= substr($characters, rand(0, strlen($characters)), 1);
        }
        $tokenString = md5($tokenString);

        $dbAdapter = $this->getServiceLocator()->get('dbAdapter');
        $tokenTable = new \Application\Model\TokenTable($dbAdapter);
        $tokenTable->insert(array(
            'ch_Servico' => $service,
            'ch_Chave' => $keyword,
            'ch_Token' => $tokenString,
            'dt_Expiracao' => $expires ? DateConvert::toDatabase($expires) : null,
        ));

        return $tokenString;
    }

    /**
     * Check if token is valid and can be used
     * 
     * @param type $service
     * @param type $keyword
     * @param type $token
     * @return boolean
     */
    public function isValid($service, $keyword, $token)
    {
        $dbAdapter = $this->getServiceLocator()->get('dbAdapter');
        $tokenTable = new \Application\Model\TokenTable($dbAdapter);

        return (bool) $tokenTable->findValidToken($service, $keyword, $token);
    }

    /**
     * Invalidate the token to prevent it from being used
     * 
     * @param string $service
     * @param string $token
     */
    public function invalidate($service, $token)
    {
        $dbAdapter = $this->getServiceLocator()->get('dbAdapter');
        $tokenTable = new \Application\Model\TokenTable($dbAdapter);

        return (bool) $tokenTable->delete(array(
            'ch_Servico' => $service,
            'ch_Token' => $token,
        ));
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

}
