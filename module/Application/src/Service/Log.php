<?php

namespace Application\Service;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * @Service
 * 
 * @author Augusto Coelho
 */
class Log implements ServiceLocatorAwareInterface
{

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * @var \Zend\Log\Logger
     */
    protected $logger;

    /**
     * @return \Zend\Log\Logger
     */
    public function getLogger()
    {
        if (!$this->logger) {
            $dbAdapter = $this->getServiceLocator()->get('dbAdapter');

            $mapping = array(
                'timestamp' => 'dt_Data',
                'priority' => 'id_LogOperacoesNivel',
                'priorityName' => 'ch_Tipo',
                'message' => 'ch_Evento',
                'extra' => array(
                    'id_Participante' => 'id_Participante',
                    'ch_IP' => 'ch_IP',
                    'additionalData' => 'ch_DadosAdicionais',
                ),
            );

            $logger = new \Zend\Log\Logger;
            $writer = new \Zend\Log\Writer\Db($dbAdapter, 'tb_log', $mapping);

            $formatter = new \Zend\Log\Formatter\Db('Y-m-d H:i:s');
            $writer->setFormatter($formatter);

            $logger->addWriter($writer);

            $this->logger = $logger;
        }

        return $this->logger;
    }

    /**
     * @param string $event
     * @param array $params
     * @param integer $level
     */
    public function log($event, array $params = null, $level = \Zend\Log\Logger::INFO)
    {
        $extra = array(
            'ch_IP' => $_SERVER['REMOTE_ADDR'],
        );

        $authService = $this->getServiceLocator()->get('AuthService');
        if ($authService->hasIdentity()) {
            $extra['id_Participante'] = $authService->getIdentity()->getId();
        }

        if (null != $params) {
            $configWriter = new \Zend\Config\Writer\Json();
            $extra['additionalData'] = $configWriter->toString($params);
        }

        $this->getLogger()->log($level, $event, $extra);
    }

    /**
     * @param \Exception $exception
     */
    public function logException(\Exception $exception)
    {
        $trace = $exception->getTraceAsString();
        $i = 1;
        do {
            $messages[] = $i++ . ": " . $exception->getMessage();
        } while ($exception = $exception->getPrevious());

        $log = "Exception:n" . implode("\n", $messages);
        $log .= "\nTrace:\n" . $trace;

        $this->log('505', array('error' => $log), \Zend\Log\Logger::EMERG);
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

}
