<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Item
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue("AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nome;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $valor;

    /**
     *  @ORM\JoinTable(name="pedido_item",
     *      joinColumns={@ORM\JoinColumn(name="qtd", referencedColumnName="item_id")},
     *      )
     */
    private $qtd;

    function getId()
    {
        return $this->id;
    }

    function getNome()
    {
        return $this->nome;
    }

    function getValor()
    {
        return $this->valor;
    }

    function getQtd()
    {
        return $this->qtd;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setNome($nome)
    {
        $this->nome = $nome;
    }

    function setValor($valor)
    {
        $this->valor = $valor;
    }

    function setQtd($qtd)
    {
        $this->qtd = $qtd;
    }

}
