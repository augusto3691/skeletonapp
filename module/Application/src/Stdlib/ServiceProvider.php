<?php

namespace Application\Stdlib;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManager;

/**
 * @see \Zend\ServiceManager\ServiceManagerAwareInterface
 * @see \Zend\EventManager\EventManagerAwareInterface
 * 
 * @author Augusto Coelho
 */
class ServiceProvider implements ServiceManagerAwareInterface, EventManagerAwareInterface
{

    /**
     * @var \Zend\ServiceManager\ServiceManager
     */
    protected $serviceManager;

    /**
     * @var \Zend\EventManager\EventManagerInterface
     */
    protected $eventsManager;

    /**
     * @param \Zend\ServiceManager\ServiceManager $serviceManager
     * @return \Application\Stdlib\Service
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    /**
     * @return zZend\ServiceManager\ServiceManager
     */
    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    /**
     * (non-PHPdoc)
     * @see Zend\EventManager.EventManagerAwareInterface::setEventManager()
     */
    public function setEventManager(EventManagerInterface $events)
    {
        $identifiers = array(__CLASS__, get_called_class());
        if (isset($this->eventIdentifier)) {
            if ((is_string($this->eventIdentifier)) || (is_array($this->eventIdentifier)) || ($this->eventIdentifier instanceof Traversable)
            ) {
                $identifiers = array_unique(array_merge($identifiers, (array) $this->eventIdentifier));
            } elseif (is_object($this->eventIdentifier)) {
                $identifiers[] = $this->eventIdentifier;
            }
            // silently ignore invalid eventIdentifier types
        }
        $events->setIdentifiers($identifiers);
        $this->eventsManager = $events;
        return $this;
    }

    /**
     * (non-PHPdoc)
     * @see Zend\EventManager.EventsCapableInterface::getEventManager()
     */
    public function getEventManager()
    {
        if (!$this->eventsManager instanceof EventManagerInterface) {
            $this->setEventManager(new EventManager());
        }
        return $this->eventsManager;
    }

}
