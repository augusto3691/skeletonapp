<?php

namespace Application\Stdlib;

/**
 * Array utils
 * 
 * @author Augusto Coelho
 */
class ArrayUtils
{

    public static function sortByColumn(&$arr, $col, $dir = SORT_ASC)
    {
        $sort_col = array();
        foreach ($arr as $key => $row) {
            $sort_col[$key] = $row[$col];
        }

        array_multisort($sort_col, $dir, $arr);
    }

}
