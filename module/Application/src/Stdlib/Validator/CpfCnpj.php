<?php

namespace Application\Stdlib\Validator;

use Zend\Validator\AbstractValidator;

/**
 * @author Augusto Coelho
 */
class CpfCnpj extends AbstractValidator
{

    const NOT_RECOGNIZED = 'notRecognized';
    const INVALID_CPF = 'invalidCpf';
    const INVALID_CNPJ = 'invalidCnpj';

    protected $messageTemplates = array(
        self::NOT_RECOGNIZED => "'%value%' não parece ser um CPF ou CNPJ válido",
        self::INVALID_CPF => "O CPF informado não parece estar correto",
        self::INVALID_CNPJ => "O CNPJ informado não parece estar correto",
    );

    protected function Cpf($value)
    {
        $igual = true;
        for ($i = 1, $t = strlen($value); $i < $t; $i++) {
            if ($value[$i - 1] != $value[$i]) {
                $igual = false;
                break;
            }
        }
        if ($igual == true) {
            return false;
        }

        $primeiroDv = $segundoDv = 0;
        for ($i = 0; $i < 9; $i++) {
            $primeiroDv += $value[$i] * (10 - $i);
        }
        for ($i = 0; $i < 10; $i++) {
            $segundoDv += $value[$i] * (11 - $i);
        }

        //calcula o primeiro dígito verificador
        $x = $primeiroDv % 11;
        if ((($x > 1) ? (11 - $x) : 0) != $value[9]) {
            return false;
        }

        //calcula o segundo dígito verificador
        $x = $segundoDv % 11;
        if ((($x > 1) ? (11 - $x) : 0) != $value[10]) {
            return false;
        }

        return true;
    }

    protected function Cnpj($value)
    {
        //calcula o primeiro dígito verificador
        $a = $i = $d1 = $d2 = 0;
        $j = 5;
        for ($i = 0; $i < 12; $i++) {
            $a += $value[$i] * $j;
            ($j > 2) ? $j-- : $j = 9;
        }
        $a = $a % 11;
        $d1 = ($a > 1) ? (11 - $a) : 0;

        //calcula o segundo dígito verificador
        $a = $i = 0;
        $j = 6;
        for ($i = 0; $i < 13; $i++) {
            $a += $value[$i] * $j;
            ($j > 2) ? $j-- : $j = 9;
        }
        $a = ($a % 11);
        $d2 = ($a > 1) ? (11 - $a) : 0;

        return (($d1 == $value[12] * 1) && ($d2 == $value[13] * 1));
    }

    public function isValid($value)
    {
        $value = preg_replace('/[^0-9]/', '', $value);
        $this->setValue($value);

        switch (strlen($value)) {
            case 11: //CPF
                if ($this->Cpf($value) === false) {
                    $this->error(self::INVALID_CPF);
                    return false;
                }
                break;
            case 14: //CNPJ
                if ($this->Cnpj($value) === false) {
                    $this->error(self::INVALID_CNPJ);
                    return false;
                }
                break;
            default:
                $this->error(self::NOT_RECOGNIZED);
                return false;
        }

        return true;
    }

}
