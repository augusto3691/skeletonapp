<?php

namespace Application\Stdlib;

use DateTime;

/**
 * @author Augusto Coelho
 */
class DateConvert
{

    /**
     * Formats a date to be sent to the database
     * 
     * @param DateTime $datetime
     * @return string Date in format Y-m-d H:i:s
     */
    public static function toDatabase(DateTime $datetime)
    {
        return $datetime->format('Y-m-d H:i:s');
    }

    /**
     * Converts a date coming from the database for an object
     * 
     * @param string $datetime Date in format Y-m-d H:i:s
     * @return \DateTime
     */
    public static function fromDatabase($datetime)
    {
        $pos = strrpos($datetime, '.');
        if ($pos > 0) {
            $datetime = substr($datetime, 0, $pos);
        }

        return DateTime::createFromFormat('Y-m-d H:i:s', $datetime);
    }

}
