<?php

namespace Application\Stdlib\Filter;

use Zend\Filter\AbstractFilter;

/**
 * @author Augusto Coelho
 */
class DateTime extends AbstractFilter
{

    protected $format;

    public function __construct($options = null)
    {
        if ($options) {
            $this->setOptions($options);
        }
    }

    public function setFormat($format)
    {
        $this->format = $format;
        return $this;
    }

    public function getFormat()
    {
        return $this->format;
    }

    public function filter($value)
    {
        if ($value && is_string($value)) {
            return \DateTime::createFromFormat($this->getFormat(), $value);
        }
    }

}
