<?php

namespace Application\Stdlib\Filter;

use Zend\Filter\AbstractFilter;

/**
 * @author Augusto Coelho
 */
class Currency extends AbstractFilter
{

    protected $decimalSeparator;

    public function __construct($options = null)
    {
        if ($options) {
            $this->setOptions($options);
        }
    }

    public function setDecimalSeparator($decimalSeparator)
    {
        $this->decimalSeparator = $decimalSeparator;
        return $this;
    }

    public function getDecimalSeparator()
    {
        return $this->decimalSeparator;
    }

    public function filter($value)
    {
        $value = preg_replace("/[^0-9{$this->getDecimalSeparator()}]/", '', $value);
        $value = preg_replace("/{$this->getDecimalSeparator()}/", '.', $value);
        return (float) $value;
    }

}
