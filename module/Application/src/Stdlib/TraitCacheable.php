<?php

namespace Application\Stdlib;

use Zend\Cache\Storage\StorageInterface;

/**
 * @Trait
 * 
 * @author Augusto Coelho
 */
trait TraitCacheable
{

    private $CACHE_KEYWORD = 'FromCache';

    /**
     * @var \Zend\Cache\Storage\StorageInterface
     */
    protected $storage;

    /**
     * @param \Zend\Cache\Storage\StorageInterface $storage
     * @return \Application\Stdlib\Cacheable
     */
    public function setCacheStorage(StorageInterface $storage)
    {
        $this->storage = $storage;
        return $this;
    }

    /**
     * @return \Zend\Cache\Storage\StorageInterface
     */
    public function getCacheStorage()
    {
        return $this->storage;
    }

    public function buildCacheHash($method, array $arguments = array())
    {
        $class = get_class($this);
        $method = str_replace($this->CACHE_KEYWORD, '', $method);

        $params = $arguments;
        foreach ($arguments as $key => $value) {
            if (is_object($value) && method_exists($value, '__toString')) {
                $params[$key] = $value->__toString();
            }
        }
        $params = var_export($params, true);

        $hash = "{$class}_{$method}_{$params}";
        $hash = str_replace(array("\r\n", "\n", "\r", "\t", " "), '', $hash);

        return md5($hash);
    }

    public function __call($name, $arguments)
    {
        $contents = null;

        if (strstr($name, $this->CACHE_KEYWORD)) {
            if (!$this->getCacheStorage()) {
                throw new \Exception("You need to set the cache storage to invoke 'FromCache' methods!");
            }

            $hash = $this->buildCacheHash($name, $arguments);
            $contents = $this->getCacheStorage()->getItem($hash);

            if (!$contents) {
                $method = str_replace($this->CACHE_KEYWORD, '', $name);
                $contents = call_user_func_array(array($this, $method), $arguments);
                $this->getCacheStorage()->setItem($hash, $contents);
            }
        }

        return $contents;
    }

}
