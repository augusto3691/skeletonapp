<?php

namespace Application\InputFilter;

use Zend\InputFilter\InputFilter;

/**
 * @InputFilter
 * 
 * @author Augusto Coelho
 */
class ChangePassword extends InputFilter
{

    public function __construct($dbAdapter)
    {
        $filterStringTrim = new \Zend\Filter\StringTrim;
        $filterStripTags = new \Zend\Filter\StripTags;

        $this->add(array(
            'name' => 'currentCredential',
            'required' => true,
            'validators' => array(
                new \Zend\Validator\StringLength(array(
                    'min' => 6,
                    'max' => 12,
                    )),
            ),
            'filters' => array(
                $filterStringTrim,
                $filterStripTags,
            ),
        ));

        $this->add(array(
            'name' => 'newCredential',
            'required' => true,
            'validators' => array(
                new \Zend\Validator\StringLength(array(
                    'min' => 6,
                    'max' => 12,
                    )),
                new \Zend\Validator\Identical('newCredentialConfirm'),
            ),
            'filters' => array(
                $filterStringTrim,
                $filterStripTags,
            ),
        ));

        $this->add(array(
            'name' => 'newCredentialConfirm',
            'required' => true,
            'validators' => array(
                new \Zend\Validator\StringLength(array(
                    'min' => 6,
                    'max' => 12,
                    )),
                new \Zend\Validator\Identical('newCredential'),
            ),
            'filters' => array(
                $filterStringTrim,
                $filterStripTags,
            ),
        ));
    }

}
