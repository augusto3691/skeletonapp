<?php

namespace Application\InputFilter;

use Zend\InputFilter\InputFilter;

/**
 * @InputFilter
 * 
 * @author Augusto Coelho
 */
class Usuario extends InputFilter
{

    public function __construct()
    {
        $filterStringTrim = new \Zend\Filter\StringTrim;
        $filterStripTags = new \Zend\Filter\StripTags;

        $this->add(array(
            'name' => 'id_saudacao',
            'required' => true,
            'validators' => array(
                new \Zend\Validator\StringLength(array(
                    'max' => 4,
                )),
            ),
            'filters' => array(
                $filterStringTrim,
                $filterStripTags,
            ),
        ));

        $this->add(array(
            'name' => 'nome',
            'required' => true,
            'validators' => array(
                new \Zend\Validator\StringLength(array(
                    'min' => 3,
                    'max' => 200,
                )),
            ),
            'filters' => array(
                $filterStringTrim,
                $filterStripTags,
            ),
        ));
        
        $this->add(array(
            'name' => 'sobrenome',
            'required' => true,
            'validators' => array(
                new \Zend\Validator\StringLength(array(
                    'min' => 3,
                    'max' => 200,
                )),
            ),
            'filters' => array(
                $filterStringTrim,
                $filterStripTags,
            ),
        ));

        $this->add(array(
            'name' => 'apelido',
            'required' => false,
            'validators' => array(
                new \Zend\Validator\StringLength(array(
                    'max' => 100,
                )),
            ),
            'filters' => array(
                $filterStringTrim,
                $filterStripTags,
            ),
        ));

        $this->add(array(
            'name' => 'sexo',
            'required' => false,
            'validators' => array(
                new \Zend\Validator\InArray(array(
                    'haystack' => array('M', 'F'),
                )),
            ),
            'filters' => array(
                $filterStringTrim,
                $filterStripTags,
            ),
        ));

        $this->add(array(
            'name' => 'cpf',
            'required' => false,
            'validators' => array(
                new \Application\Stdlib\Validator\CpfCnpj,
            ),
            'filters' => array(
                $filterStringTrim,
                $filterStripTags,
            ),
        ));

        $this->add(array(
            'name' => 'data_nascimento',
            'required' => false,
            'validators' => array(
                new \Zend\Validator\Date(array(
                    'format' => 'd/m/Y',
                )),
            ),
            'filters' => array(
                new \Application\Stdlib\Filter\DateTime(array(
                    'format' => 'd/m/Y',
                )),
            ),
        ));

        $this->add(array(
            'name' => 'id_cargo_titulo',
            'required' => false,
            'validators' => array(
                new \Zend\Validator\StringLength(array(
                    'max' => 2,
                )),
            ),
            'filters' => array(
                $filterStringTrim,
                $filterStripTags,
            ),
        ));

        $this->add(array(
            'name' => 'id_cargo_funcao',
            'required' => false,
            'validators' => array(
                new \Zend\Validator\StringLength(array(
                    'max' => 4,
                )),
            ),
            'filters' => array(
                $filterStringTrim,
                $filterStripTags,
            ),
        ));

        $this->add(array(
            'name' => 'ddd_telefone',
            'required' => true,
            'validators' => array(
                new \Zend\Validator\StringLength(array(
                    'max' => 2,
                )),
                new \Zend\Validator\Digits,
            ),
            'filters' => array(
                $filterStringTrim,
                $filterStripTags,
            ),
        ));

        $this->add(array(
            'name' => 'telefone',
            'required' => true,
            'validators' => array(
                new \Zend\Validator\StringLength(array(
                    'min' => 9,
                    'max' => 9,
                )),
            ),
            'filters' => array(
                $filterStringTrim,
                $filterStripTags,
            ),
        ));

        $this->add(array(
            'name' => 'ramal',
            'required' => false,
            'validators' => array(
                new \Zend\Validator\StringLength(array(
                    'max' => 10,
                )),
            ),
            'filters' => array(
                $filterStringTrim,
                $filterStripTags,
            ),
        ));

        $this->add(array(
            'name' => 'ddd_celular',
            'required' => true,
            'validators' => array(
                new \Zend\Validator\StringLength(array(
                    'max' => 2,
                )),
            ),
            'filters' => array(
                $filterStringTrim,
                $filterStripTags,
            ),
        ));

        $this->add(array(
            'name' => 'celular',
            'required' => true,
            'validators' => array(
                new \Zend\Validator\StringLength(array(
                    'min' => 9,
                    'max' => 10,
                )),
            ),
            'filters' => array(
                $filterStringTrim,
                $filterStripTags,
            ),
        ));
    }

}
