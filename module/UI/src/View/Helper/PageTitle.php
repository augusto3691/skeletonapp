<?php

namespace UI\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * @ViewHelper
 * 
 * @author Augusto Coelho
 */
class PageTitle extends AbstractHelper implements ServiceLocatorAwareInterface
{

    /**
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function __invoke($pageTitle, $titleDescription)
    {
        $params = array();
        $params['pageTitle'] = $pageTitle;
        $params['titleDescription'] = $titleDescription;

        return $this->getView()->render('helper/pageTitle', $params);
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

}
