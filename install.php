<?php

//functions
if (!function_exists('readline')) {

    function readline($prompt = null)
    {
        if ($prompt) {
            echo $prompt;
        }
        $fp = fopen("php://stdin", "r");
        $line = rtrim(fgets($fp, 1024));
        return $line;
    }

}

//get php version
$phpversion = phpversion();

//create a config array
$configArray = [];

//user variables
print_r("*********************************************************\n");
$applicationName = readline("Application Name? ");
print_r("*********************************************************\n");
$dbHost = readline("Data Base Host? ");
print_r("*********************************************************\n");
$dbName = readline("Data Base Name? ");
print_r("*********************************************************\n");
$dbUserName = readline("Data Base User? ");
print_r("*********************************************************\n");
$dbPassword = readline("Data Base Pass? ");
print_r("*********************************************************\n");

//populate config array with user variables
$configArray['db']['driver'] = 'Pdo';
$configArray['db']['dsn'] = 'mysql:dbname=' . $dbName . ';host=' . $dbHost;
$configArray['db']['user'] = $dbUserName;
$configArray['db']['password'] = $dbPassword;
$configArray['db']['driver_options'] = array(
    1002 => 'SET NAMES \'UTF8\''
);

// check if autoload directory is writable
if (is_writable('config/autoload')) {
    file_put_contents("config/autoload/local.php", "<?php
return " . var_export($configArray, true) . ";");
} else {
    echo "<pre>";
    print_r("The folder config/autoload does not have write permission.");
    die();
}
print_r("*********************************************************\n");
print_r("Config Local File Created! \n");
print_r("*********************************************************\n");

print_r("*********************************************************\n");
print_r("Running Database Migration... \n");
print_r("*********************************************************\n");

//Migrate Database
shell_exec("php migrate.php -m {$dbHost} -d {$dbName} -u {$dbUserName} -p {$dbPassword} data/skeletonapp.sql");

print_r("*********************************************************\n");
print_r("Database Created! \n");
print_r("*********************************************************\n");


print_r("*********************************************************\n");
print_r("Running Composer Install... \n");
print_r("*********************************************************\n");

//execute composer
shell_exec('php composer.phar install');

print_r("*********************************************************\n");
print_r("All Dependencies Installed! \n");
print_r("*********************************************************\n");

print_r("*********************************************************\n");
print_r("Application Installed! \n");
print_r("*********************************************************\n");
